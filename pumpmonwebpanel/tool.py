#    pumpmonwebpanel - Comet-ME Water pump web monitoring
#    Copyright (C) 2018 Comet-ME
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

# coding: utf-8
import glob
from os import environ, path
from argparse import ArgumentParser

import pandas as pd
from pumpmonwebpanel import average
import pumpmonwebpanel.indexer as indexer


PUMPMON_LOGS = environ.get('PUMPMON_LOGS', 'testlogs')


def summarize_daily_flow():
    parser = ArgumentParser()
    parser.add_argument('-r', '--root', default=PUMPMON_LOGS)
    parser.add_argument('-o', '--output', default=path.join(PUMPMON_LOGS, 'daily_flow.csv'))
    args = parser.parse_args()
    if not path.exists(args.root):
        print("no such directory or not enough permissions: {}".format(args.root))
        return
    pumpfiles = glob.glob(path.join(args.root, 'pump*_????.csv'))
    cat = average.catalog_df(pumpfiles)
    data = list(average.itercsvs(cat))
    dfs = [x[2] for x in data]
    df = pd.concat(dfs)
    dfs = df[[indexer.FLOW_VARIABLE_NAME]].resample('60s').mean().resample('1d').sum()
    dfs.index = [x.date() for x in dfs.index]
    dfs.rename(columns={'controller.stats.flow_rate_running_average.average': 'Flow Sum in Liters'}, inplace=True)
    dfs.index.name = 'Date'
    dfs.to_csv(args.output)


if __name__ == '__main__':
    summarize_daily_flow()
