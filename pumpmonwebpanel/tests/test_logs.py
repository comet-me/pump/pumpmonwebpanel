from datetime import datetime
from mock import patch

# TODO:
#from .util import mock_time

from pumpmonwebpanel.logs import BackingOffStat

@patch('pumpmonwebpanel.logs.now')
@patch('pumpmonwebpanel.logs.os_stat')
def test_sanity(patched_stat, patched_now):
    patched_now.return_value = datetime(2000, 1, 1)
    obj = BackingOffStat()
    class Dummy:
        pass
    patched_stat.return_value = Dummy()
    first = obj('a')
    patched_now.return_value = datetime(2000, 1, 1, 1)
    patched_stat.return_value = Dummy()
    second = obj('a')
    assert first != second
    assert list(sorted(obj.last.keys())) == ['a']
    vals = list(sorted(obj.last.values()))
    assert len(vals) == 1
    assert vals[0][0] == 0
    assert vals[0][2] == second
    patched_now.return_value = datetime(2000, 1, 1, 2)
    third = obj('a')
    vals = list(sorted(obj.last.values()))
    assert len(vals) == 1
    assert vals[0][0] == BackingOffStat.initial_non_zero_seconds
    assert vals[0][2] == second
    exp = BackingOffStat.initial_non_zero_seconds
    for i in range(10):
        patched_now.return_value = datetime(2000, 1, 1, 3 + i)
        fourth = obj('a')
        vals = list(sorted(obj.last.values()))
        assert len(vals) == 1
        assert vals[0][0] == min(exp * 2, BackingOffStat.max_seconds)
        assert vals[0][2] == second
        exp = exp * 2
