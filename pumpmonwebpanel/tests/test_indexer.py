from pdb import set_trace as b
import pytest
from contextlib import contextmanager
import os
from sys import stderr
from tempfile import TemporaryDirectory
from datetime import datetime, timedelta
from os.path import join
from os import stat
from glob import glob
from mock import patch


from tzlocal import get_localzone
from pytz import timezone, FixedOffset
import pandas as pd

from ..indexer import todays_cols_sampled_60s, Averager, main_react
from ..logs import SUFFIX, LOGS_PATH_ENV


utc = timezone('UTC')


def generate_cat_filename(cat, creation_timestamp):
    t = creation_timestamp
    year, month, day, hour, minute = (t.year, t.month, t.day, t.hour, t.minute)
    return '{cat}_{year:04}{month:02}{day:02}_{hour:02}{minute:02}{SUFFIX}'.format(
            SUFFIX=SUFFIX, cat=cat, year=year, month=month, day=day, hour=hour, minute=minute
        )


@contextmanager
def create_single_day_data(cat, minutes_of_data, fields, tz, start=None):
    with TemporaryDirectory() as d:
        os.environ[LOGS_PATH_ENV] = d
        if start is None:
            start = datetime.now()
        filename = make_fake_cat_file(d=d, cat=cat, minutes_of_data=minutes_of_data, fields=fields,
                                      tz=tz, timestamp=start)
        os.chdir(d)
        os.mkdir('cache')
        yield dict(d=d, start=start, filename=filename)


def make_fake_cat_file(d, cat, minutes_of_data, fields, tz, timestamp):
    seconds_from_start_of_day = 3600 * 12 - (minutes_of_data * 60 // 2)
    hour = seconds_from_start_of_day // 3600
    minute = (seconds_from_start_of_day - 3600 * hour) // 60
    second = seconds_from_start_of_day - 3600 * hour - 60 * minute
    year = timestamp.year
    month = timestamp.month
    day = timestamp.day
    start = datetime(year=year, month=month, day=day,
            hour=hour, minute=minute, second=second, tzinfo=tz)
    filename = join(d, generate_cat_filename(cat=cat, creation_timestamp=start))
    timestamps = [(start + timedelta(seconds=1) * i).timestamp() * 1000 for i in range(minutes_of_data * 60)]
    data = {'timestamp': timestamps}
    for field in fields:
        data[field] = [42] * len(timestamps)
    df = pd.DataFrame(data)
    df.to_csv(filename, index=False)
    return filename


def duplicate_last_line(filename):
    # bad implementation, good enough for test
    with open(filename) as fd:
        lines = fd.readlines()
    with open(filename, 'w+') as fd:
        fd.writelines(lines + lines[-1:])
        fd.flush()


def create_updating_data(cat, initial_minutes_of_data, fields, tz,
                         number_of_additional_files,
                         number_of_seconds_to_run,
                         start, stage_callback=None):
    with create_single_day_data(cat=cat, minutes_of_data=initial_minutes_of_data,
                                fields=fields, tz=tz, start=start) as oneday:
        d, start_ts, updating_filename = oneday['d'], oneday['start'], oneday['filename']
        # generate older files
        one_day = timedelta(days=1)
        filenames = {updating_filename}
        for i in range(1, 1 + number_of_additional_files):
            newfile = make_fake_cat_file(d=d, cat=cat, minutes_of_data=initial_minutes_of_data,
                               fields=fields, tz=tz, timestamp=start_ts - i * one_day)
            filenames.add(newfile)
        assert len(filenames) == 1 + number_of_additional_files
        # First time - initial data
        yield d
        if callable(stage_callback):
            stage_callback()
        for i in range(number_of_seconds_to_run):
            # just update the newest file every time, add a row
            duplicate_last_line(updating_filename)
            yield d
            if callable(stage_callback):
                stage_callback()


class CallCounter:
    def __init__(self, f):
        self.f = f
        self.count = 0
        self.calls = []

    def reset(self):
        ret = self.count
        self.count = 0
        return ret

    def __call__(self, *args, **kw):
        self.calls.append((args, kw))
        ret = self.f(*args, **kw)
        self.count += 1
        return ret


@patch('pumpmonwebpanel.logs.now')
@patch('pumpmonwebpanel.logs.os_stat', new_callable=lambda: CallCounter(stat))
@patch('pumpmonwebpanel.logs.glob', new_callable=lambda: CallCounter(glob))
def test_performance(counting_glob, counting_stat, mock_now):
    # For 10 seconds for 5 files with a single file being updated, there should be stat
    # system calls of:
    # 5 + 5 + 8
    # First batch sees baseline for all
    # Second batch sees change to only one
    # The unchanged 8 are not polled again for the next 10 seconds, resulting in no check
    # for the next 8 checks

    results = []
    second = timedelta(seconds=1)
    start = [datetime(2012, 12, 31, 5, 51, 0)]
    mock_now.return_value = start[0]

    def after_stage():
        results.append((counting_glob.reset(), counting_stat.reset()))
        start[0] += second
        mock_now.return_value = start[0]

    main_react(create_updating_data(cat='pump', initial_minutes_of_data=100, fields=['a'],
                                    tz=utc, number_of_additional_files=4,
                                    number_of_seconds_to_run=120,
                                    start=start[0], stage_callback=after_stage),
               sys_args=[])
    # TODO - glob cache too? - or just change to check every 5 seconds
    # TODO - switch to inotify later. will give the same benetifs, without backoff logic
    assert results[-1] == (1, 1)


def debug(p):
    print(f'debug: {p}', file=stderr)


def test_todays_cols_sampled_60s():
    cat = 'pump'
    fields = ['afield']
    tz = FixedOffset(3*60)
    for m, ret_minutes in [(1, 2), (100, 100), (1440, 1440)]:
        with create_single_day_data(cat=cat, minutes_of_data=m, fields=fields, tz=tz) as oneday:
            d = todays_cols_sampled_60s(cat, fields)
            debug(f'created {d}')
            assert set(d.keys()) == set(fields)
            for k in fields:
                assert d[k].shape == (ret_minutes,) # we are on a half minute divide


def test_averager():
    cat = 'pump'
    fields = ['afield']
    tz = FixedOffset(3*60)
    with create_single_day_data(cat=cat, minutes_of_data=100, fields=fields, tz=tz) as oneday:
        d = oneday['d']
        df = todays_cols_sampled_60s(cat, fields)[fields[0]]
        assert (df.index[-1] - df.index[0]).total_seconds() == 5940.0
        averager = Averager(initdf=df, field=fields[0], cat=cat, timestamp='timestamp')

