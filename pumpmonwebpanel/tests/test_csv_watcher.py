from os import path
from tempfile import TemporaryDirectory
from ..csv_watcher import CSVWatcher


def append(filename, contents):
    with open(filename, 'a+') as fd:
        fd.write(contents)


def test_csv_watcher():
    watcher = None
    with TemporaryDirectory() as tempdir:
        filename = path.join(tempdir, 'file.csv')
        append(filename, '')
        watcher = CSVWatcher(open(filename))
        for state, newlines in [
                ('1,2\n', [['1', '2']]),
                ('3', []),
                (',4', []),
                ('\n5', [['3', '4']]),
                (',6\n', [['5', '6']]),
                ('7,8\n9,10\n11,12\n13', [['7', '8'], ['9', '10'], ['11', '12']]),
                (',14\n', [['13', '14']])
            ]:
            append(filename, state)
            assert list(watcher.read()) == newlines

        # test breaking at one point
        csv = "\n".join(str(i) for i in range(10)) + '\n'
        for i in range(len(csv)):
            parts = csv[:i], csv[i:]
            filename = path.join(tempdir, f'file_{i}.csv')
            append(filename, parts[0])
            watcher = CSVWatcher(open(filename))
            lines1 = list(watcher.read())
            append(filename, parts[1])
            lines2 = list(watcher.read())
            assert csv == '\n'.join(','.join(l) for l in (lines1 + lines2) + [''])

