from tempfile import TemporaryDirectory
from os import chdir, path, makedirs
from os.path import dirname
from datetime import date, datetime, timedelta
from pytz import timezone, UTC
from contextlib import contextmanager

import pandas as pd

from ..average import average, average_daily_hack, csv_files_from_root

# TODO: missing tests (known things fixed but not checked for)
# non 0 index timestamp field for pump, for mppt
# column with non numeric value
# display_transform
# average after display_transform

ts_20180311 = int(datetime(year=2018,month=3,day=11,hour=6,tzinfo=UTC).timestamp() * 1000)
sec = 1000

ts = ts_20180311
ts_2 = ts + 86400 * 1000
pump_contents = [
    ('pump_20180311_1.csv', [
        'timestamp,A',
        f'{ts},10',
        f'0', # should be dropped
        f'{ts + sec},20',
        f'{ts + 2 * sec},30',
        f'{ts + 3 * sec},40']),
    ('pump_20180311_2.csv', [
        'timestamp,A',
        f'{ts + 4*sec},50',
        f'{ts + 5*sec},60',
        f'{ts + 6*sec},70',
        f'{ts + 7*sec},80',
        ]),
    ('pump_20180312.csv', [
        'timestamp,A',
        f'{ts_2 + sec * 10},90',
        f'{ts_2 + sec * 11},100',
        f'{ts_2 + sec * 12},110',
        f'{ts_2 + sec * 13},120',
        ]),
]
mppt_contents = [
    ('mppt_20180311_1.csv', [
        #'0', # should be dropped - pandas.read_csv doesn't handle it, we need to add 'drop_part_before_header' functionality
        'RecordedTime,B',
        f'{ts + sec * 0.1},5',
        f'{ts + sec * 1.1},10',
        f'{ts + sec * 2.2},20',
        f'{ts + sec * 3.3},30',
    ]),
    ('mppt_20180311_2.csv', [
        'RecordedTime,B',
        f'{ts + sec * 4.4},35',
        f'{ts + sec * 5.5},40',
        f'{ts + sec * 6.6},45',
        f'{ts + sec * 7.7},50',
    ]),
    ('mppt_20180312.csv', [
        'RecordedTime,B',
        f'{ts_2 + sec * 10.1},55',
        f'{ts_2 + sec * 11.1},60',
        f'{ts_2 + sec * 12.2},65',
        f'{ts_2 + sec * 13.3},70',
    ]),
]

tests_data_no_filter = [
    # start, end, averaging, result file
    ((2019, 1, 1), (2019, 1, 1), 1.0,
        [],
        [
        ]),
    ((2018, 3, 11), (2018, 3, 12), 1.0,
        ['timestamp', 'A', 'B'],
        [
            ['2018-03-11 06:00:00+00:00', 10.0, 5.0],
            ['2018-03-11 06:00:01+00:00', 20.0, 10.0],
            ['2018-03-11 06:00:02+00:00', 30.0, 20.0],
            ['2018-03-11 06:00:03+00:00', 40.0, 30.0],
            ['2018-03-11 06:00:04+00:00', 50.0, 35.0],
            ['2018-03-11 06:00:05+00:00', 60.0, 40.0],
            ['2018-03-11 06:00:06+00:00', 70.0, 45.0],
            ['2018-03-11 06:00:07+00:00', 80.0, 50.0],
            ['2018-03-12 06:00:10+00:00', 90.0, 55.0],
            ['2018-03-12 06:00:11+00:00', 100.0, 60.0],
            ['2018-03-12 06:00:12+00:00', 110.0, 65.0],
            ['2018-03-12 06:00:13+00:00', 120.0, 70.0],
        ]),
    ((2018, 3, 11), (2018, 3, 12), 2.0,
        ['timestamp', 'A', 'B'],
        [
            ['2018-03-11 06:00:00+00:00', 15.0, 7.5],
            ['2018-03-11 06:00:02+00:00', 35.0, 25.0],
            ['2018-03-11 06:00:04+00:00', 55.0, 37.5],
            ['2018-03-11 06:00:06+00:00', 75.0, 47.5],
            ['2018-03-12 06:00:10+00:00', 95.0, 57.5],
            ['2018-03-12 06:00:12+00:00', 115.0, 67.5],
        ]
        ),
    ]

test_headers_whitelist = {
    'mppt': {'RecordedTime':1, 'B':1},
    'pump': {'timestamp': 1}
}

tests_data_with_filter = [
    # start, end, averaging, result file
    ((2019, 1, 1), (2019, 1, 1), 1.0,
        [],
        [
        ]),
    ((2018, 3, 11), (2018, 3, 12), 1.0,
        ['timestamp', 'B'],
        [
        ['2018-03-11 06:00:00+00:00', 5.0],
        ['2018-03-11 06:00:01+00:00', 10.0],
        ['2018-03-11 06:00:02+00:00', 20.0],
        ['2018-03-11 06:00:03+00:00', 30.0],
        ['2018-03-11 06:00:04+00:00', 35.0],
        ['2018-03-11 06:00:05+00:00', 40.0],
        ['2018-03-11 06:00:06+00:00', 45.0],
        ['2018-03-11 06:00:07+00:00', 50.0],
        ['2018-03-12 06:00:10+00:00', 55.0],
        ['2018-03-12 06:00:11+00:00', 60.0],
        ['2018-03-12 06:00:12+00:00', 65.0],
        ['2018-03-12 06:00:13+00:00', 70.0],
        ]),
    ((2018, 3, 11), (2018, 3, 12), 2.0,
        ['timestamp', 'B'],
        [
        ['2018-03-11 06:00:00+00:00', 7.5],
        ['2018-03-11 06:00:02+00:00', 25.0],
        ['2018-03-11 06:00:04+00:00', 37.5],
        ['2018-03-11 06:00:06+00:00', 47.5],
        ['2018-03-12 06:00:10+00:00', 57.5],
        ['2018-03-12 06:00:12+00:00', 67.5],
        ]),
    ]


@contextmanager
def environment(mppt, pump):
    with TemporaryDirectory() as tmpdir:
        chdir(tmpdir)
        for filename, contents in (mppt_contents if mppt else []) + (pump_contents if pump else []):
            with open(filename, 'w+') as fd:
                fd.write('\n'.join(contents) + '\n')
        yield tmpdir


def test_average():
    for data, headers_whitelist in [
        (tests_data_no_filter, None),
        (tests_data_with_filter, test_headers_whitelist)
        ]:
        for pump, mppt in [(True, True), (True, False), (False, True)]:
            with environment(pump=pump, mppt=mppt) as tmpdir:
                for (s_y, s_m, s_d), (e_y, e_m, e_d), period, exp_header, exp_events in data:
                    # Average
                    start_date = datetime(year=s_y, month=s_m, day=s_d)
                    end_date = datetime(year=e_y, month=e_m, day=e_d)
                    df = average(start_date=start_date, end_date=end_date,
                                 period_seconds=period, root=tmpdir, use_display_map=False,
                                 tz=timezone('UTC'), headers_whitelist=headers_whitelist)
                    header = ([df.index.name] if df.index.name is not None else []) + df.columns.tolist()
                    rows = [[str(row[0])] + row[1].tolist() for row in df.iterrows()]
                    if pump and mppt:
                        assert(exp_header == header)
                        assert(exp_events == rows)
                    elif not pump:
                        assert df.empty
                    else:
                        # expect just missing values for B
                        assert([x for x in exp_header if x != 'B'] == header)



def test_average_daily_hack():
    for data, headers_whitelist in [
        (tests_data_no_filter, None),
        (tests_data_with_filter, test_headers_whitelist)
        ]:
        with environment(pump=True, mppt=True) as tmpdir:
            for (s_y, s_m, s_d), (e_y, e_m, e_d), period, exp_header, exp_events in data:
                start_date = datetime(year=s_y, month=s_m, day=s_d)
                end_date = datetime(year=e_y, month=e_m, day=e_d)
                base = average(start_date=start_date, end_date=end_date,
                               period_seconds=period, root=tmpdir, use_display_map=False,
                               tz=timezone('UTC'), headers_whitelist=headers_whitelist)
                dest = path.join(tmpdir, 'hack.csv')
                average_daily_hack(dest=dest, start_date=start_date, end_date=end_date,
                               period_seconds=period, root=tmpdir, use_display_map=False,
                               tz=timezone('UTC'), headers_whitelist=headers_whitelist)
                try:
                    result = pd.read_csv(dest, parse_dates=['timestamp']).set_index('timestamp')
                except:
                    result = pd.DataFrame()
                # we are getting a different kind of timestamp - just silence it
                base.index = base.index.astype(result.index.dtype)
                if not result.equals(base):
                    import pdb; pdb.set_trace()
                    assert False


def create_files(l):
    for p, contents in l:
        d = dirname(p)
        if not path.exists(d):
            makedirs(d)
        with open(p, 'w+') as fd:
            fd.write(contents)


def test_subdir_crawl():
    with TemporaryDirectory() as d:
        create_files([('2020/10/10/pump_20201010_1010.csv', 'a,b\n1,2\n')])
        files = list(csv_files_from_root(d))
        assert len(files) == 1
        assert files[0].endswith('2020/10/10/pump_20201010_1010.csv')

if __name__ == '__main__':
    test_average_daily_hack()
