import pytest
from ..sql import SQL


@pytest.mark.skip("information_schema.columns table is assumed by SQL.rebuild, which does not exist under sqlite; try to use sqlalchemy.MetaData.reflection")
def test_sanity():
    sql = SQL(connection='sqlite://', use_partition=False)
    sql.rebuild()


if __name__ == '__main__':
    test_sanity()
