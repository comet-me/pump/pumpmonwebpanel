from os import environ


def setup_environment():
    environ['PUMPMON_VARS'] = 'no vars'
    environ['PUMPMON_ELF'] = 'no elf'


setup_environment()


from ..consts import MPPT_CAT, PUMP_CAT
from ..main import get_display_data


def test_get_display_data():
    data = {}
    res = get_display_data(data)

    data = {MPPT_CAT: {'Pi': 42, 'Po': 20}}
    res = get_display_data(data)
    assert res == {MPPT_CAT: [('Power input', '42.0'), ('Power output', '20.0')]}

    data = {MPPT_CAT: {'Po': 42, 'Pi': 20}}
    res = get_display_data(data)
    assert res == {MPPT_CAT: [('Power input', '20.0'), ('Power output', '42.0')]}
