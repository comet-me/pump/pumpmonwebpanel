#!/usr/bin/env python3

from pdb import set_trace as b
import re
from os.path import join, basename, exists
from os import makedirs
from glob import  glob
from shutil import move
from argparse import ArgumentParser


rexp = re.compile('(\w+)_(?P<year>[0-9][0-9][0-9][0-9])(?P<month>[0-9][0-9])(?P<day>[0-9][0-9]).*\.csv')
# pump_20100102_0154.csv

def iter_filenames(d):
    for f in glob(join(d, '*.csv')):
        filename = basename(f)
        res = rexp.match(filename)
        if res is None:
            continue
        d = res.groupdict()
        year, month, day = d['year'], d['month'], d['day']
        if year is None or month is None or day is None:
            continue
        yield f, year, month, day


def main():
    parser = ArgumentParser()
    parser.add_argument('-d', '--dir', default='.')
    parser.add_argument('-r', '--really', default=False, action='store_true')
    args = parser.parse_args()
    for f, year, month, day in iter_filenames(args.dir):
        dest = join(args.dir, year, month, day)
        if not args.really:
            print('test: {f} => {dest}'.format(f=f, dest=dest))
        else:
            if not exists(dest):
                makedirs(dest)
            move(f, join(dest, basename(f)))


if __name__ == '__main__':
    main()
