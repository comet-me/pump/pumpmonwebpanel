#    pumpmonwebpanel - Comet-ME Water pump web monitoring
#    Copyright (C) 2018 Comet-ME
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

from csv import reader


class CSVWatcher:
    def __init__(self, fd):
        self.fd = fd
        self.partial_line = []
        self.unread = []

    def read(self):
        """
        generator of lines, will stop if there are no more lines
        """
        def csvparse(line):
            return list(reader([line]))[0]
        while True:
            newbuf = self.fd.readline()
            # shortcut
            if len(self.partial_line) == 0 and newbuf.endswith('\n'):
                yield csvparse(newbuf)
                continue
            self.partial_line.append(newbuf)
            if self.partial_line[-1].endswith('\n'):
                newline = ''.join(self.partial_line)
                del self.partial_line[:]
                yield csvparse(newline)
                continue
            break

