#!/usr/bin/env python3

#    pumpmonwebpanel - Comet-ME Water pump web monitoring
#    Copyright (C) 2018 Comet-ME
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

from os import path, stat, makedirs, stat, listdir, environ
from os.path import join, isdir, dirname
from sys import stderr, stdout
from datetime import date, datetime, timedelta
from time import sleep
from collections import defaultdict
from string import ascii_letters
from io import StringIO
from tempfile import TemporaryDirectory
import shutil
import gc
import resource
from multiprocessing import Process
from contextlib import contextmanager
from logging import getLogger

from pdb import set_trace as b
import csv
import pandas as pd
import numpy as np
from celery import Celery
from celery.__main__ import main as celery_main_helper

from tzlocal import get_localzone
from pytz import timezone


logger = getLogger('average')

try:
    import tables
    tables_supported = True
except:
    tables_supported = False

from . import consts
from .logs import basename_to_timestamp

# TODO - use logging
debug = False

# Must be defined in the outside environment, i.e. via env file in systemd
slug = environ.get('PUMPMON_PANEL_SLUG', 'origin')

celeryapp = Celery(
    'pumpmonwebpanel.average',
    broker='pyamqp://guest@localhost',
    )
# IMPORTANT: run as uid != 0, or you have to set C_FORCE_ROOT
celeryapp.conf.task_serializer = 'pickle'
celeryapp.conf.result_serializer = 'pickle'
celeryapp.conf.accept_content = ['pickle']
celeryapp.conf.task_default_queue = 'pumpmonwebpanel.{}'.format(slug)


DEBUG_DATES = 'dates'
DEBUG_FILE = 'file'
DEBUG_FILENAME = 'filename'
DEBUG_PROGRESS = 'progress'
DEBUG_STATS = 'stats'
DEBUG_MEMORY = 'memory'
DEBUG_SECTIONS = [DEBUG_FILE, DEBUG_FILENAME, DEBUG_PROGRESS, DEBUG_STATS, DEBUG_MEMORY, DEBUG_DATES]
debug_sections = {d: False for d in DEBUG_SECTIONS}


def eprint(s, section=None, **kw):
    if section is not None and not debug_sections[section]:
        return
    print(s, file=stderr, **kw)


@contextmanager
def debug_memory(text=''):
    pre = resource.getrusage(resource.RUSAGE_SELF).ru_maxrss
    yield
    post = resource.getrusage(resource.RUSAGE_SELF).ru_maxrss
    eprint("memory: {}: {}: {} => {}".format(text, post - pre, pre, post), section=DEBUG_MEMORY)


def banner(s, file=stderr):
    print(s, file=file)
    print('=' * len(s), file=file)


BAD_DATE = datetime.utcnow() + timedelta(days=1)

def dateparse(millis, tz):
    """
    fits DataFrame.read_csv date_parser argument

    convert a UTC timestamp to the timezone in question.

    i.e. for a timezone of +300 the hour will be shifted 3 forward
    and the datetime object will have tzinfo=@tz
    """
    ret = []
    for milli in millis:
        try:
            milli = float(milli)
        except:
            # return a value which we later filter on. It cannot be nan, there will
            # be complaints. We rely on the millis value
            ret.append(BAD_DATE)
            continue
        val = tz.fromutc(datetime.utcfromtimestamp(milli / 1000.0))
        ret.append(val)
    return ret


def basename_to_timestamp_column(basename):
    # TODO - use the column with a specific header, and use a header cache?
    first_part = basename.split('_')[0]
    return {'mppt': 2, 'pump': 0, 'flow': 0}[first_part]


def isheader(cols):
    return all(c[:1] in ascii_letters for c in cols)


def prelim_csv_check(filename, maxlines=10):
    skiprows = []
    reason = "no header in first {} lines".format(maxlines)
    with open(filename) as fd:
        c = csv.reader(fd)
        for i in range(maxlines):
            try:
                l = next(c)
            except StopIteration:
                reason = "no data in file"
                break
            if not isheader(l):
                if len(skiprows) == 0:
                    return dict(go=False, skiprows=[], header=-1, reason="missing header")
                return dict(go=True, skiprows=skiprows[:-1], header=0)
            if isheader(l):
                skiprows.append(i)
    return dict(go=False, skiprows=[], header=-1, reason=reason)


def reverse_dict(d):
    ret = defaultdict(list)
    for k, v in d.items():
        ret[v].append(k)
    return ret


def one_header(filename):
    header = None
    lines = []
    with open(filename) as fd:
        for raw_line in fd:
            if '\00' in raw_line:
                continue
            try:
                l = list(csv.reader([raw_line]))[0]
            except csv.Error as e:
                logger.error(e)
                continue
            if isheader(l):
                if header is None:
                    header = l
            else:
                lines.append(l)
    s = StringIO()
    w = csv.writer(s)
    w.writerows([header])
    w.writerows(lines)
    s.seek(0)
    return s


numerals = set(range(10))
def is_numeric(s):
    return set(s) <= numerals


def get_cache_dir_from_log_dir(dirname):
    # remove YYY/MM/DD dirs
    parts = dirname.rsplit('/', 3)
    if len(parts) == 4 and all(is_numeric, parts[1:]):
        dirname = parts[0]
    cache_dir = join(dirname, 'cache')
    return cache_dir


def cached_read_csv(filename, **kw):
    """
    Cache file to HDF store.
    Except if file was updated today.
    """
    basename = path.basename(filename)
    noext, ext = path.splitext(basename)
    dirname = path.dirname(filename)
    cache_dir = get_cache_dir_from_log_dir(dirname)
    if not path.exists(cache_dir):
        makedirs(cache_dir)
    hdf = join(cache_dir, '{}.hdf'.format(noext))
    stat_file = stat(filename)
    today = datetime.now().date()
    file_touched_today = datetime.fromtimestamp(stat_file.st_mtime).date() == today
    if not file_touched_today and tables_supported and path.exists(hdf) and stat(hdf).st_ctime > stat_file.st_mtime:
        if debug:
            eprint("reading hdf cached file {}".format(hdf), section=DEBUG_FILE)
        return pd.read_hdf(hdf, 'dataframe')
    ret = pd_read_csv(filename, **kw)
    if tables_supported and not file_touched_today:
        if debug:
            eprint("saved hdf cached file {}".format(hdf), section=DEBUG_FILE)
        ret.to_hdf(hdf, 'dataframe')
    return ret


def pd_read_csv(*args, **kw):
    df = pd.read_csv(*args, **kw)
    return df


def read_csv(filename, tz=None):
    ct = catalog_df([filename])
    files = list(itercsvs(ct, tz=tz))
    if len(files) == 0:
        return None
    _a, _b, df = files[0]
    return df


def csv_files_from_root(root):
    return files_with_suffix_from_root(root, '.csv')


def files_with_suffix_from_root(root, suffix):
    """
    Despite the name, return files that only conform to specific directories:
    either at the root (to keep working before the switch to YYYY/MM/DD
    or under YYYY/MM/DD
    """
    level_one = listdir(root)
    for one in level_one:
        full_one = join(root, one)
        if isdir(full_one) and len(one) == 4:
            for two in listdir(full_one):
                full_two = join(full_one, two)
                if isdir(full_two) and len(two) == 2:
                    for three in listdir(full_two):
                        full_three = join(full_two, three)
                        if isdir(full_three) and len(three) == 2:
                            for f in listdir(full_three):
                                if f.endswith(suffix):
                                    yield join(full_three, f)
        elif one.endswith(suffix):
            yield join(root, one)


def catalog_df(root_or_list, time_columns_names=None, verbose=False):
    """
    read first line of each file, look for a timestamp.
    run time estimate: O(#files * #columns * head_count)
    start with head_count = 4 - we have a double header bug in some files
    """

    if time_columns_names is None:
        time_columns_names = {'RecordedTime', 'timestamp'}

    eprint('catalog_df: {}'.format(repr(root_or_list)), section=DEBUG_FILENAME)
    ds = []
    head_count = 4
    columns = ['stamp', 'filename', 'timestamp_col', 'columns']
    if isinstance(root_or_list, str):
        files = list(csv_files_from_root(root_or_list))
    else:
        files = root_or_list
    for f in files:
        with open(f) as fd:
            found, dt = None, None
            for _i in range(head_count):
                candidate = next(csv.reader([fd.readline()]))
                if isheader(candidate):
                    columns = candidate
                    for col_i, col in enumerate(columns):
                        if col in time_columns_names:
                            found = col_i
                            break
                    else:
                        found = 0
                    break
            if found is None:
                basename = path.basename(f)
                if verbose:
                    eprint("warning: {}: falling back to filename for timestamp column".format(basename), section=DEBUG_FILENAME)
                found = basename_to_timestamp_column(basename)
            if dt is None:
                try:
                    dt = basename_to_timestamp(path.basename(f))
                except:
                    eprint("skipping {} due to missing timestamp in filename".format(f), section=DEBUG_FILENAME)
                    continue
            ds.append(dict(stamp=dt, filename=f, timestamp_col=found, columns=columns))
    df = pd.DataFrame(ds)
    if df.shape[0] == 0:
        return pd.DataFrame(columns=columns)
    eprint('catalog df {}'.format(df.shape), section=DEBUG_FILENAME)
    df = df.sort_values(by='stamp')
    df['name'] = df.filename.map(lambda x: path.basename(x))
    return df


def itercsvs(file_df, usecols=None, tz=None, verbose=False):
    """
    TODO: use usecols: filtering at read time is better for performance

    yields (category, basename), tup, df
    """
    if tz is None:
        tz = get_localzone()

    for filename_i, tup in enumerate(file_df.itertuples()):
        filename, timestamp_col = tup.filename, tup.timestamp_col
        basename = path.basename(filename)
        category, filename_rest = basename.split('_', 1)
        if verbose:
            banner("reading {}: {}".format(category, filename_rest))
        else:
            eprint("\r{} / {}".format(filename_i + 1, len(file_df)), end='', section=DEBUG_PROGRESS)
        check = prelim_csv_check(filename)
        if not check['go']:
            if verbose:
                eprint("warning: skipping {} because {}".format(filename, check['reason']), section=DEBUG_FILENAME)
            continue
        parse_csv_kw = dict(index_col=timestamp_col,
                parse_dates=True, date_parser=lambda millis: dateparse(millis, tz=tz),
                error_bad_lines=False # drops too long lines
        )
        try:
            df = cached_read_csv(filename, header=check['header'], skiprows=check['skiprows'], **parse_csv_kw)
        except TypeError: # assume because of multiple headers in file, remove them via generator
            single_header = one_header(filename)
            if single_header is None:
                continue # error should have been logged by one_header
            df = pd_read_csv(single_header, **parse_csv_kw)
        yield (category, basename), tup, df
        del df


def list_to_front(l, front):
    """
    Sort elements in iterable front to front of l, not changing
    anything else.
    """
    s = set(l)
    first = [f for f in front if f in s]
    fs = set(first)
    last = [c for c in l if c not in fs]
    return first + last


def file_size(filename):
    return stat(filename).st_size


def average_append(dest, **kw):
    with debug_memory('average'):
        df = average(**kw)
    if path.exists(dest) and file_size(dest) > 0:
        append_df_to_csv(df, dest)
    else:
        df.to_csv(dest)


def average(start_date, end_date, period_seconds, root, catalog=None, tz=None, headers_whitelist=None,
            use_display_map=True, verbose=False, time_columns_names=None,
            average_period_minimal_fraction=0.0, drop_below_zero=False,
            remove_under_voltage=False):
    """
    average_period_minimal_fraction = f:
        for any period [T..T+period) for basic sampling time T we expect at least
        T * f samples or we drop it
    """

    timestamp_label = 'timestamp'

    # assume files are same date.
    # assume files are linear wrt time.
    period = period_seconds
    if catalog is not None:
        file_df_full = catalog
    else:
        file_df_full = catalog_df(root, time_columns_names, verbose=verbose)

    file_df = file_df_full.copy()
    if verbose:
        banner("verbose: Candidate files", file=stderr)
        eprint(file_df)

    # Filter on dates
    # TODO: fix this - hack to get correct results
    stamp = file_df['stamp'].apply(lambda x: x.date())
    if isinstance(end_date, datetime):
        end_date = end_date.date()
    if isinstance(start_date, datetime):
        start_date = start_date.date()
    cond = (stamp >= start_date) & (stamp <= end_date)
    file_df = file_df[cond]

    if len(file_df) == 0:
        return pd.DataFrame()

    eprint('{} .. {}: {}'.format(start_date, end_date, file_df.shape), section=DEBUG_DATES)

    if verbose:
        banner("Averaged and merged files ({}, {})".format(start_date, end_date))
        eprint(file_df[['stamp', 'name', 'timestamp_col']])
    else:
        eprint("averaging {} files".format(len(file_df)), section=DEBUG_STATS)

    input_period = period
    period = int(period)
    period = max(1, period)
    if period != input_period:
        eprint("warning: period truncated to {}".format(period), section=DEBUG_FILE)
    period_num = period
    period = '{}s'.format(period)     # offset string for Pandas

    # maintain a running set of columns. always use the initial columns
    # warn when we drop columns
    columns = None
    # doing this all at once - for a short period it will be expensive
    frames = defaultdict(list)
    drop_cols = {
        'flow': [],
        'pump': [],
        'mppt': ['Unparsed']
    }

    it = itercsvs(file_df, usecols=headers_whitelist, tz=tz, verbose=verbose)
    period_cutoff = period_num * average_period_minimal_fraction
    for filename_i, ((category, basename), col, df) in enumerate(it):
        if not isheader(df.columns.tolist()):
            eprint("quitting due to missing header, please fix: {}".format(basename), DEBUG_FILE)
            return pd.DataFrame()
        df = df.drop(drop_cols[category], axis=1, errors='ignore') # columns not supported < 0.21
        df = df.dropna() # drop lines that were too short so contained empty values
        if len(df) == 0:
            if verbose:
                eprint("skipping empty file {}".format(basename))
            continue
        # crop at zero.
        # Note: can't use df[df < 0] = 0 because some columns are not numeric
        for col in df.columns:
            if df[col].dtype not in [np.float, np.float64, np.int64, np.int]:
                continue
            df.loc[df[col] < 0, col] = 0
        resamp = df.resample(period)
        dfmean = resamp.mean()
        dfcount = resamp.count()
        df = dfmean[dfcount >= period_cutoff].dropna()
        if len(df) == 0:
            if verbose:
                eprint("skipping empty after resampling {}".format(basename), section=DEBUG_FILE)
                continue
        df.index.name = timestamp_label
        # zero values where the output power is below zero
        # try to avoid breaking if column name changes
        if drop_below_zero and consts.PUMP_AVERAGE_POWER_VARNAME in df.columns:
            PAP = consts.PUMP_AVERAGE_POWER_VARNAME
            df[df[PAP] < 0.0][PAP] = 0.0
        frames[category].append(df)
        gc.collect()
    totalrows = sum([sum([df.shape[0] for df in values]) for values in frames.values()])
    eprint("total rows in read files (after resampling and nans): {}".format(totalrows), section=DEBUG_STATS)
    dfs = {category: pd.concat(dfs, sort=True) for category, dfs in frames.items()}
    dfs = list(dfs.values())
    df = dfs[0]
    for other in dfs[1:]:
        df = pd.merge(left=df, right=other, left_index=True, right_index=True, how='outer')
    eprint("\ndebug: {} rows, {} cols".format(len(df), len(df.columns)), section=DEBUG_STATS)
    if headers_whitelist:
        cols_before = len(df.columns)
        have_cols = set(df.columns)
        good_cols = sum([list(vs.keys()) for vs in headers_whitelist.values()], [])
        new_cols = [c for c in df.columns if c in good_cols]
        df = df[new_cols]
        cols_after = len(df.columns)
        eprint("headers filter: {} => {}".format(cols_before, cols_after), section=DEBUG_STATS)
    if use_display_map:
        # multiples are summed
        flat_rev = reverse_dict(consts.display_mapping_flat)
        for k, vs in flat_rev.items():
            if any(v not in df.columns for v in vs):
                if verbose:
                    eprint("debug: display mapping ignored: {} => {}".format(repr(vs), k), section=DEBUG_FILE)
                continue
            df[k] = df[vs].max(axis=1)
            df = df.drop(labels=set(vs) - {k}, axis=1)
        # set excel readable strings (hopefully)
        df.index = df.index.map(lambda x: '{:%Y-%m-%d %H:%M:%S}'.format(x))
        if verbose:
            eprint("before post display map dropna: {}".format(df.shape), section=DEBUG_STATS)
        df = df.dropna(how='all')
        if verbose:
            eprint("after post display map dropna: {}".format(df.shape), section=DEBUG_STATS)

    # replace 0 read flow with NaN - then dropna will remove the rows
    measured_flow_label = 'Measured Flow LPM'
    df.loc[df[measured_flow_label] <= 0] = np.NaN

    # remove any rows with no values - they are meaningless for further processing,
    # they just indicate there were readings in this time, but there was nothing read,
    # mainly because of no sun -> no pump readings
    df = df.dropna(how='all', subset=list(set(df.columns) - {timestamp_label}))

    if remove_under_voltage:
        # There is a bug where we are dropping all the MPPT values because Tmp is missing. This needs fixing
        # Alternatively this would still make sense because MPPT could not have been logged even if it was operational,
        # or we could be running from wall power
        if 'Flgs.UV' in df.columns:
            df = df.loc[df['Flgs.UV'].isna() | (df['Flgs.UV'] == 0.0) ]

    return df


def from_date_str(date_str, start, tz):
    year, month, day = date_str.split('-')
    if start:
        hour, minute = 0, 0
    else:
        hour, minute = 23, 59
    return datetime(year=int(year), month=int(month), day=int(day), hour=hour, minute=minute, tzinfo=tz)


def move_file(src, dest):
    if isinstance(dest, str):
        shutil.move(src, dest)
    else:
        with open(src, 'r') as fd:
            for line in fd:
                dest.write(line)


def append_df_to_csv(df, filename):
    """
    Append df to filename
    if columns match - just append to file
    otherwise use the subset of columns existing.

    TODO: add new columns at the end. Requires rewriting the first row.
    """
    with open(filename) as fd:
        headers = list(next(csv.reader(fd)))
    if headers != df.columns.tolist():
        # TODO: rewrite first line of file
        columns = set(df.columns)
        headers_subst = [h for h in headers if h in columns]
        df = df[headers_subst]
    with open(filename, 'a') as fd:
        df.to_csv(fd, header=False)


def FakeProcess(target, kwargs):
    class Process:
        def start(self):
            target(**kwargs)
        def join(self):
            pass
    return Process()



def average_daily_hack(dest, start_date, end_date, **kw):
    """
    Until the memory requirement is fixed, limit merging to a single day,
    then merge those.
    """
    def days(start_date, end_date):
        d = start_date
        one = timedelta(days=1)
        while d + one < end_date:
            yield d, d + one
            d += one
        yield d, end_date
    with TemporaryDirectory() as temp_dir:
        filename = join(temp_dir, 'total.csv')
        for s, e in days(start_date, end_date):
            p = Process(target=average_append, kwargs=dict(dest=filename, start_date=s, end_date=e, **kw))
            p.start()
            p.join()
        move_file(filename, dest)


@celeryapp.task
def celery_average(start_date, end_date, period_seconds, root, headers_whitelist,
                   target_filename, remove_under_voltage):
    print("{}, {}, {}, {}, {}".format(repr(start_date), repr(end_date), repr(period_seconds),
            repr(root), repr(headers_whitelist)), file=stderr)
    assert isinstance(start_date, datetime), "unexpected: {}, expected datetime".format(repr(start_date))
    assert isinstance(end_date, datetime), "unexpected: {}, expected datetime".format(repr(end_date))
    if not path.exists(path.dirname(target_filename)):
        makedirs(path.dirname(target_filename))
    df = average(
        start_date=start_date, end_date=end_date,
        period_seconds=period_seconds,
        root=root,
        headers_whitelist=headers_whitelist,
        remove_under_voltage=remove_under_voltage,
    )
    with TemporaryDirectory() as temp_dir:
        temp = join(temp_dir, 'averaged.csv')
        df.to_csv(temp)
        move_file(temp, target_filename)


def celery_main():
    celery_main_helper()


def main():
    from argparse import ArgumentParser, ArgumentTypeError

    def DirType(s):
        if not path.isdir(s):
            raise ArgumentTypeError('{} is not a directory'.format(s))
        return s

    parser = ArgumentParser()
    parser.add_argument('-i', '--interval', type=float, default=60.0, help="seconds")
    parser.add_argument('-p', '--path', type=DirType, default='.')
    parser.add_argument('-t', '--today', action='store_true', default=False)
    parser.add_argument('-d', '--debug', action='store_true', default=False)
    parser.add_argument('--debug-section', choices=DEBUG_SECTIONS, metavar='debug_section', action='append', default=[])
    parser.add_argument('-s', '--start', default=None)
    parser.add_argument('-e', '--end', default=None)
    parser.add_argument('-z', '--time-zone', default=None)
    parser.add_argument('-H', '--hack', default=False, action='store_true')
    parser.add_argument('--debug-process', default=False, action='store_true')
    args = parser.parse_args()

    if args.debug_process:
        globals()['Process'] = FakeProcess
    if args.debug:
        for section in DEBUG_SECTIONS:
            debug_sections[section] = True
    for debug_section in args.debug_section:
        if debug_section in DEBUG_SECTIONS:
            debug_sections[debug_section] = True
    if len(args.debug_section) > 0:
        args.debug = True

    if args.time_zone is None:
        tz = get_localzone()
    else:
        tz = timezone(args.time_zone)

    if args.today:
        start_date = end_date = datetime.now(tz=tz)
        start_date = start_date.replace(hour=0, minute=0)
        end_date = end_date.replace(hour=23, minute=59)
    elif args.start and args.end:
        start_date, end_date = from_date_str(args.start, start=True, tz=tz), from_date_str(args.end, start=False, tz=tz)
    else:
        start_date, end_date = datetime(year=1900, month=1, day=1), datetime(year=2100, month=1, day=1)
    globals()['debug'] = args.debug
    with debug_memory('all'):
        if not args.hack:
            df = average(
                start_date=start_date,
                end_date=end_date,
                period_seconds=args.interval,
                root=args.path,
                tz=tz,
                headers_whitelist=consts.headers_whitelist,
                verbose=args.debug
                )
            df.to_csv(stdout)
        else:
            average_daily_hack(
                dest=stdout,
                start_date=start_date,
                end_date=end_date,
                period_seconds=args.interval,
                root=args.path,
                tz=tz,
                headers_whitelist=consts.headers_whitelist,
                verbose=args.debug
                )


if __name__ == '__main__':
    main()
