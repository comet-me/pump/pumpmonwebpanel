#!/bin/env python3

#    pumpmonwebpanel - Comet-ME Water pump web monitoring
#    Copyright (C) 2018 Comet-ME
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

from datetime import datetime
from io import StringIO
from os import path, unlink
from glob import glob
from argparse import ArgumentParser
from time import sleep, time
from collections import defaultdict


from .logs import last_files, prefixes, suffix, MAIN_PREFIX


def endless_lines(fd):
    lines = []
    for line in fd:
        yield line
        lines.append(line)
    i = 0
    while True:
        yield lines[i]
        i = (i + 1) % len(lines)


digits = set('0123456789')


def take_while(it, cond):
    ret = [next(it)]
    while True:
        if not cond(ret[-1]):
            return ret
        ret.append(next(it))


assert(take_while(iter([1,2,3]), lambda c: c % 2 == 0) == [1])
assert(take_while(iter([1,2,3]), lambda c: c % 2 == 1) == [1, 2])


def yield_rows_and_keep_time(prefix, line_iter):
    parser = Parser(prefix)
    time = None
    unyielded = []
    while True:
        line = next(line_iter)
        row = parser.parse(line)
        if row.time is not None:
            time = row.time
        if time is not None and row.time is None:
            row.time = time
        if time is not None:
            for row_pre in unyielded:
                row_pre.time = time
                yield row_pre
            unyielded.clear()
            yield row
        else:
            unyielded.append(row)


class Row:
    def __init__(self, prefix, raw, time, rest):
        self.is_header = raw[0] not in digits
        self.prefix = prefix
        self.raw = raw
        if self.is_header:
            self.time = None
        else:
            self.time = float(time)
        self.rest = rest

    def is_header_or_before(self, next_time):
        #print('debug: {next_time!r} ? {self.time!r} ( header = {self.is_header} )')
        return self.is_header or next_time > self.time


class Parser:
    prefix_to_timeindex = dict(mppt=0, pump=2)

    def __init__(self, prefix):
        self.prefix = prefix
        self.timeindex = self.prefix_to_timeindex[prefix]

    def parse(self, line):
        parts = line.split(',')
        return Row(prefix=self.prefix, raw=line, time=parts[self.timeindex], rest=parts[1:])


class Output:

    def __init__(self, prefix, output):
        self.output = output

    def write(self, l):
        self.output.write(l)
        self.output.flush()


class Clock:
    def __init__(self, start, factor=5, maxsleep=1000):
        self.maxsleep = maxsleep
        self.start = start
        self.time = self.start
        self.wall_start = time()
        self.factor = factor

    def sleep_to(self, next_time): # in milliseconds
        if next_time > self.time:
            orig_dt = (next_time - self.time) / self.factor / 1000.0
            dt = min(orig_dt, self.maxsleep)
            dt_str = '{dt} ({orig_dt}'.format(**locals()) if dt != orig_dt else str(dt)
            print('sleep {dt_str}: {time} => {next_time} [x{factor}]'.format(dt_str=dt_str, time=self.time, next_time=next_time, factor=self.factor))
            self.time = next_time
            sleep(dt)
        return next_time


def group_in_dict(l, key_f):
    ret = defaultdict(list)
    for v in l:
        ret[key_f(v)].append(v)
    return ret


def main():
    parser = ArgumentParser()
    parser.add_argument('--source', default='logs')
    parser.add_argument('--target', default='.')
    parser.add_argument('--suffix', default=suffix)
    #parser.add_argument('--delay', default=1, type=float)
    parser.add_argument('--verbose', action='store_true', default=False)
    parser.add_argument('--dryrun', action='store_true', default=False)
    parser.add_argument('--maxsleep', type=float, default=5.0, help='truncate sleeps to this value')
    args = parser.parse_args()

    sources = glob(path.join(args.source, '*' + str(args.suffix)))
    if len(sources) == 0:
        raise Exception("no files in {src}.*{suf}".format(src=args.source, suf=args.suffix))
    print(sources)
    sources = last_files(sources, prefixes)
    files = list(sources.values())
    # banner
    print("replay {' & '.join(files)} to {}".format(args.target))
    today_str = datetime.now().strftime('%Y%m%d_%H%M%S')
    targets = {prefix: path.join(args.target, '{prefix}_{today_str}.csv'.format(prefix=prefix, today_str=today_str)) for prefix in prefixes}
    for prefix, target in targets.items():
        if path.exists(target):
            print("removing {}".format(target))
            if not args.dryrun:
                unlink(target)
    outs = {
        prefix: Output(prefix, StringIO() if args.dryrun else open(target, 'w+'))
        for prefix, target in targets.items()
    }
    ins = {
        prefix: open(source) for prefix, source in sources.items()
    }
    count = 0
    iters = {prefix: yield_rows_and_keep_time(prefix, endless_lines(inp)) for prefix, inp in ins.items()}

    non_main_prefixes = list(sorted(set(prefixes) - set([MAIN_PREFIX])))

    first_group = take_while(
        iters[MAIN_PREFIX],
        lambda row: row.is_header)
    for l in first_group[:-1]:
        outs[MAIN_PREFIX].write(l.raw)
    clock = Clock(first_group[-1].time, maxsleep=args.maxsleep)

    while True:
        base = next(iters[MAIN_PREFIX])
        next_time = base.time
        next_items_per_prefix = {
            prefix: take_while(iters[prefix], lambda r: r.is_header_or_before(next_time))
            for prefix in non_main_prefixes
        }
        items = group_in_dict(sum(next_items_per_prefix.values(), []), lambda x: x.time)
        for t in sorted(items.keys()):
            clock.sleep_to(t)
            for row in items[t]:
                if args.verbose:
                    print('write {pref}: {raw}'.format(pref=row.prefix, raw=row.raw.strip()))
                outs[row.prefix].write(row.raw)
        clock.sleep_to(next_time)
        if args.verbose:
            print('write {pref}: {raw}'.format(pref=MAIN_PREFIX, raw=base.raw.strip()))
        outs[MAIN_PREFIX].write(base.raw)
        if args.verbose:
            print(str(count))
        count += 1


if __name__ == '__main__':
    main()
