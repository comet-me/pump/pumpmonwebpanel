#    pumpmonwebpanel - Comet-ME Water pump web monitoring
#    Copyright (C) 2018 Comet-ME
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

from argparse import ArgumentParser
from sys import stderr, exit, argv
from os import environ
from os.path import realpath, basename, join, exists
from pickle import dump, load
from datetime import datetime, timedelta
import json
import shutil
from tempfile import TemporaryDirectory

from tzlocal import get_localzone
import pandas as pd
import numpy as np
#from pyinotify import # TODO

from .consts import PUMP_CAT, MPPT_CAT, FLOW_CAT
from .logs import (raw_last_data, logs_path, last_files_of_logspath, basename_to_timestamp)
# pandas used to get initial flow data when starting at middle of day
# TODO: pandas loading is slow on RPi
from .average import read_csv
from .sql import SQL, get_connection
from .util import max_or_none


debug = False

# This is used by pumpmonwebpanel, identical to the logs
FLOW_VARIABLE_NAME = 'controller.stats.flow_rate_running_average.average'
MPPT_POWER_OUT_VARIABLE_NAME = 'Po'

STATE_BASE = 'last_data.pickle'
def get_state_file():
    return join(logs_path(), 'cache', STATE_BASE)

# XXX: this can be done with a memory mapped array - but is it worth if?
# just 24*60 = 1440 entries array, so 10 Kilobyte - just repickle it every
# second
PER_MINUTE_BASE = 'per_minute.pickle'
PER_MINUTE_FILE = join(logs_path(), 'cache', PER_MINUTE_BASE)

averages = [
    (PUMP_CAT, FLOW_VARIABLE_NAME, 'flow'),
    (MPPT_CAT, MPPT_POWER_OUT_VARIABLE_NAME, 'power')
]


def isfloat(x):
    try:
        f = float(x)
        return True
    except:
        return False


def last_data(source=None):
    if source is None:
        source = get_state_file()
    if not exists(source):
        d = raw_last_data()
        d['averages'] = [('LastMinute', '0')] + [('{}Daily'.format(param.capitalize()), '0.0') for a_, b_, param in averages]
        return d
    with open(source, 'rb') as fd:
        return load(fd)


def last_averages_per_minute(source=PER_MINUTE_FILE):
    if not exists(source):
        return {}
    with open(source, 'rb') as fd:
        return load(fd)


def dump_averages_per_minute(averagers, target):
    dailies = {b: avg.daily.tolist() for b, avg in averagers.items()}
    replace_file(target, lambda fd: dump(dailies, fd))


last_state = None
def dump_state(state, target):
    global last_state
    if last_state == state:
        return
    last_state = state
    replace_file(target, lambda fd: dump(state, fd))


def replace_file(target, writer):
    with TemporaryDirectory() as d:
        temp_filename = join(d, 'temp.bin')
        with open(temp_filename, 'wb+') as fd:
            writer(fd)
        shutil.move(temp_filename, target)


def localtime_from_utctimestamp(milliseconds, tz):
    return tz.fromutc(datetime.utcfromtimestamp(milliseconds / 1000.0))


def todays_cols_sampled_60s(cat, fields, tz=None):
    """
    Return last 24 hours, which is always today and the day before
    """
    if tz is None:
        tz = get_localzone()
    empty = {field: None for field in fields}
    last = last_files_of_logspath(2)
    if cat not in last:
        # no file yet for category
        return empty
    last_csv_filenames = last[cat]
    if debug:
        print("debug: {}: reading {}".format(cat, ', '.join(last_csv_filenames)), file=stderr)
    # skip files from yesterday
    now = datetime.now(tz=tz)
    dfs = [read_csv(filename, tz=tz) for filename in last_csv_filenames]
    dfs = [df for df in dfs if df is not None]
    if len(dfs) == 0:
        return empty
    df = pd.concat(dfs, sort=False)
    df = df[df.index >= now - timedelta(days=1)]
    if df.empty:
        return empty
    df = df.sort_index()
    return {field: df[field].resample('60s').mean() for field in fields}


class Averager:
    def __init__(self, initdf, field, cat, timestamp, tz=None):
        self.cat = cat
        self.timestamp_field = timestamp
        self.field = field
        self.daily = np.array([np.nan] * (24 * 60))
        self.last_start_of_day = None
        self.last_minute_values = []

        # larger than valid daily minutes
        self.last_minute = (24 * 60 * 60) + 1

        self.last_minute_avg = 0
        self.last_minute_count = 0
        self.last_timestamp = None
        if tz is None:
            tz = get_localzone()
        self.tz = tz
        self._initialize(initdf)

    def _initialize(self, dfmin):
        if dfmin is None:
            return
        daily = dfmin.values.ravel((-1, 1))
        ts_start = dfmin.index[0]
        self.last_start_of_day = ts_start.date()
        ts_start_of_day = ts_start.replace(hour=0, minute=0, second=0, microsecond=0)
        start_minute = int((ts_start - ts_start_of_day).total_seconds() / 60)
        try:
            self.daily[start_minute:start_minute + len(daily)] = daily
        except:
            print('error: could not initialize daily value; ignoring', file=stderr)
        self.last_minute = start_minute + len(daily) - 1

        # TODO - this is wrong Can be gotten before resampling
        self.last_minute_count = 1
        self.last_minute_avg = daily[-1]

    def _on_new_minute(self, minute):
        if self.last_minute == minute:
            return
        if self.last_minute > minute:
            # new day - clear everything later
            print("{cat}: minute went backwards: {last_minute} => {minute}".format(
                cat=self.cat,
                last_minute=self.last_minute,
                minute=minute), file=stderr)
            self.daily[minute:] = np.nan
        self.last_minute = minute
        self.last_minute_avg = 0
        self.last_minute_count = 0
        self.last_minute_values = []

    def _on_new_start_of_day(self, start_of_day):
        if self.last_start_of_day is None:
            self.last_start_of_day = start_of_day
            return
        if self.last_start_of_day == start_of_day:
            return
        # day change, do nothing - we change on minute going backwards
        print("{cat}: day change: {last_start_of_day} => {start_of_day}".format(
            cat=self.cat,
            last_start_of_day=self.last_start_of_day,
            start_of_day=start_of_day), stderr)
        self.last_start_of_day = start_of_day

    def total_daily(self):
        return np.nansum(self.daily)

    def update(self, data):
        # TODO: quit when day changes - makes all the logic simpler; rely on
        # external restart logic (systemd / other)
        if self.cat not in data:
            # no updated data, ignore
            return
        data = dict(data[self.cat])
        try:
            utctimestamp = float(data[self.timestamp_field])
        except:
            # non floating point - happens when header is read (TODO - fix at # source)
            return
        ts = localtime_from_utctimestamp(utctimestamp, self.tz)
        if ts == self.last_timestamp:
            # require progress: ignore repeat messages
            return
        # ignore yesterday's data
        if ts.day != datetime.now().day:
            return
        self.last_timestamp = ts
        start_of_day = ts.date()
        minute = ts.hour * 60 + ts.minute
        second = ts.second
        self._on_new_start_of_day(start_of_day)
        self._on_new_minute(minute)
        # we are in the right day, and the right minute - accumulate

        # TODO: place in consts so it is easier to change
        if self.field not in data:
            print("WARNING: no {field} in read data: {data}".format(field=self.field, data=data))
            return
        if not isfloat(data[self.field]):
            print("WARNING: non floating point value found for {field}: {value}".format(
                field=self.field,
                value=data[self.field]))
            return
        val = float(data[self.field])

        self.update_average(second, val)
        self.daily[self.last_minute] = self.last_minute_avg
        if debug:
            print('{cat}: {minute}, {last_minute_avg} #: {last_minute_count}'.format(
                cat=self.cat,
                minute=ts.minute,
                last_minute_avg=self.last_minute_avg,
                last_minute_count=self.last_minute_count,
            ), file=stderr)

    def update_average(self, second, val):
        self.last_minute_count += 1
        N = self.last_minute_count
        self.last_minute_values.append((second, val))     # debug
        self.last_minute_avg = (self.last_minute_avg * (N - 1) + val) / N


# TODO: 'flow' and 'Daily' keys should be recorded in the server/client protocol,
# probably consts.py;
# They are part of the contract API between the server and client.

def update_from_last_data(averagers, data):
    for basefield, avg in averagers.items():
        avg.update(data)
    first_avg = next(iter(averagers.values()))
    valid_last_minutes = [avg.last_minute for avg in averagers.values() if avg.last_minute < 86400]
    data['averages'] = results = [
        ('LastMinute', str(max_or_none(valid_last_minutes))),
    ]
    for basefield, avg in averagers.items():
        start = basefield.capitalize()
        results.append(('{}Daily'.format(start), str(avg.total_daily())))
        last = json.dumps(avg.daily[avg.last_minute - 2: avg.last_minute + 1].tolist())
        results.append(('{}Latest'.format(start), last))
    return data


def update_sql(sql, data):
    if sql is None:
        return
    global sql_failed
    if sql_failed:
        return
    # TODO - this should contain everything - i.e. raw_last_data should return
    # everything from last time, not just the last row. But since we are dealing
    # with averages this is not that bad for now, and perfectly fine for 1
    # minute averages.
    #sql.con.execute('insert into mppt  ...
    # TODO: nice to use DataFrame? or overkill?
    try:
        pass
    except:
        sql_failed = True


def dump_averagers(averagers):
    for k, avg in averagers.items():
        print(k)
        def render(num):
            if np.isnan(num):
                return ' '
            if num <= 0.01:
                return '_'
            return '#'
        for i in range(24):
            print('{:02} '.format(i) + ''.join(render(avg.daily[i*60 + j]) for j in range(60)))



def parse_args(sys_args):
    parser = ArgumentParser()
    parser.add_argument('-l', '--logs-path', default=None)
    parser.add_argument('-d', '--debug', action='store_true', default=False)
    parser.add_argument('-p', '--per-minute-file', default=PER_MINUTE_FILE)
    parser.add_argument('-s', '--state-file', default=None)
    parser.add_argument('--update-sql', action='store_true', default=False, help='TODO')
    parser.add_argument('--debug-set', help='give a x,y,x,y,x,y interpolated string for the flow and power')
    parser.add_argument('--debug-dump', action='store_true', help='give a x,y,x,y,x,y interpolated string for the flow and power')
    args = parser.parse_args(sys_args)
    if args.state_file is None:
        args.state_file = get_state_file()
    if args.logs_path is not None:
        environ['PUMPMON_LOGS'] = args.logs_path
    return args


def main_react(time_event_generator, sys_args):
    global debug
    args = parse_args(sys_args)
    debug = args.debug
    # TODO: inotify based, not sleep (although 0.5 second sleep is probably good
    # enough)
    pump_vars = [var for (cat, var, b) in averages if cat == PUMP_CAT]
    mppt_vars = [var for (cat, var, b) in averages if cat == MPPT_CAT]
    pumpdf = todays_cols_sampled_60s(PUMP_CAT, pump_vars)
    mpptdf = todays_cols_sampled_60s(MPPT_CAT, mppt_vars)
    cat_to_df = {PUMP_CAT: pumpdf, MPPT_CAT: mpptdf}
    cat_to_timestamp = {PUMP_CAT: 'timestamp', MPPT_CAT: 'RecordedTime'}
    averagers = {}
    for cat, field, basefield in averages:
        averagers[basefield] = Averager(field=field, cat=cat, initdf=cat_to_df[cat][field], timestamp=cat_to_timestamp[cat])
    if args.debug_set is not None:
        return update_from_piece_wise_linear(averagers, args.debug_oneoff, args.per_minute_file)
    if args.debug_dump:
        return dump_averagers(averagers)
    if args.update_sql:
        sql = SQL(get_connection())
    else:
        sql = None
    for e in time_event_generator:
        data = raw_last_data()
        update_from_last_data(averagers, data)
        dump_state(data, target=args.state_file)
        dump_averages_per_minute(averagers, target=args.per_minute_file)
        update_sql(sql, data)
        if args.debug:
            print(last_data(source=args.state_file))
            print(last_averages_per_minute(source=args.per_minute_file))


def every_dt(min_dt, max_dt):
    from time import sleep, time
    while True:
        round_start = time()
        yield
        sleep(max(min_dt, min(max_dt, max_dt - (time() - round_start))))


def main():
    main_react(every_dt(min_dt=0.5, max_dt=1.0), sys_args=argv[1:])


if __name__ == '__main__':
    main()
