#!/usr/bin/env python3

#    pumpmonwebpanel - Comet-ME Water pump web monitoring
#    Copyright (C) 2018 Comet-ME
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""
New averaging method:

dump everything into database.

next steps:
update indexer to write to db. should be fast enough.
update averager to query db. no need for celery in this case? or perhaps still
use celery and have it write the averaged results to csv cache, and then we
can download them?

bugs:
does not handle files with multiple headers correctly: averager gets over them.
should merge the functionality from it (csv reading and geussing columns).

maybe have declarative syntax of columns somewhere. perhaps consts. then
can forgoe the whole stage

pandas.read_csv: maybe supply a patch to skip bad lines (esp. in python engine)
which are repeat headers? or just in general?
"""

from io import StringIO
import argparse
import pwd
from sys import stderr
import pdb; b = pdb.set_trace
from glob import glob
from os import path, getuid
from contextlib import contextmanager
from time import time
from collections import defaultdict
import numpy as np
from datetime import timedelta, datetime
import pickle

from tzlocal import get_localzone

import pandas as pd

import sqlalchemy


@contextmanager
def timeit(txt):
    start = time()
    yield
    end = time()
    print('{txt}: {diff}'.format(txt=txt, diff=end - start))


convertions = defaultdict(int)


def tryfloat(col):
    def tryfloat(x):
        if x is None or x.strip() == '':
            return np.nan
        # better: take initial part
        try:
            return float(x)
        except:
            convertions[col] += 1
            if convertions[col] > 10:
                b()
            return np.nan
    return tryfloat


BAD_DATE = datetime.utcnow() + timedelta(days=100)

def dateparse(micros, tz):
    """
    fits DataFrame.read_csv date_parser argument
    """
    ret = []
    for micro in micros:
        try:
            val = tz.fromutc(datetime.fromtimestamp(float(micro) / 1000.0))
        except:
            # return a value which we later filter on. It cannot be nan, there will
            # be complaints. We rely on the micros value
            val = BAD_DATE
        ret.append(val)
    return ret


tz = get_localzone()

faults = defaultdict(list)

mppt_dtypes = {}
mppt_dtypes['Unparsed'] = str
mppt_dtypes['State'] = str
#mppt_dtypes['Tmp'] = np.float64
mppt_converters = {}
mppt_converters['Tmp'] = tryfloat('Tmp')
pump_dtypes = {}
pump_dtypes['controller.state.motor_state'] = str

SAMPLE_TS = 'sample_ts'

categories = ['pump', 'mppt']

timestamp_dtype = np.dtype('<M8[ns]')

pandas_to_postgres_type = {
    timestamp_dtype: 'timestamp', # timestamp with time zone',
    np.float: 'double precision',
    np.object: 'text',
    str: 'text',
    np.dtype('O'): 'text',
    np.dtype(np.bool): 'boolean',
}



def to_sql_column(col):
    return col.lower().replace('.', '_')


def get_schema(con, table_name):
    res = con.execute("select column_name, data_type from information_schema.columns where table_name = '{}'".format(table_name))
    return list(res)


def is_float_dtype(dtype):
    return dtype == np.float or dtype == np.int64


def partition_dates(start_date, end_date):
    d = start_date
    one = timedelta(days=1)
    while d + one < end_date:
        yield d, d + one
        d += one
    yield d, d + one


def null_cleaned_file(filename):
    # note: suboptimal - reads in one go
    with open(filename) as fd:
        text = fd.read()
        text = text.replace('\00', '')
    return StringIO(text)


def breakcheck(f):
    try:
        return f()
    except Exception as e:
        b()
    except:
        print('break check catch')
        b()

class SQL:
    category_to_timestamp_col = {
        'mppt': 'RecordedTime',
        'pump': 'timestamp',
    }

    def __init__(self, connection, use_partition=True):
        self.use_partition = use_partition
        self.engine = sqlalchemy.create_engine(connection)
        self.con = self.engine.connect()
        self.columns = {} # prefix to columns dictionary (ordered)
        self.created_partition = {}

    def removeall(self):
        ourtables = [name for name in self.engine.table_names() if name.startswith('mppt') or name.startswith('pump')]
        for t in ourtables:
            self.drop(t)

    def drop(self, table_name):
        print("dropping {table_name}".format(**locals()), file=stderr)
        self.con.execute('drop table {table_name} cascade'.format(**locals()))


    def _create_views_for_averaging(self, seconds):
        """
        create view mppt_15 as select avg(vi) as vi, ... , avg(tmp) as tmp, max(recordedtime) - min(recordedtime) as interval, count(*) as samples from (select *, (extract(hour from recordedtime) * 4 + (cast(extract(minute from recordedtime) as int) / 15)) as fifteen from mppt20180319) as grouped group by fifteen;
        create view avg_15 as select pump_15 inner join mppt_15
        drop not a number

        hmm. create a view for every partition? then we could have stayed with no partitions.
        but then again, nice to have a single table, no?
        """
        categories = ['mppt', 'pump']
        schemas = {pref: get_schema(self.con, pref) for pref in categories}
        schemas = {pref: schema for pref, schema in schemas.items() if len(schema) > 0}
        categories = schemas.keys()
        dp_cols = {pref: [col for col, data_type in schema if data_type == 'double precision'] for pref, schema in schemas.items()}
        for pref in categories:
            grouped = 'select *, sample_ts::date as date, (cast (extract(hour from {SAMPLE_TS}) * 3600 + extract(minute from {SAMPLE_TS}) * 60 + extract(second from {SAMPLE_TS}) as int) / {seconds}) as period from {pref}'.format(**locals())
            schema = schemas[pref]
            avg_columns = ', '.join('avg({col}) as {col}'.format(**locals()) for col in dp_cols[pref])
            self.con.execute('create view {pref}_{seconds} as select {avg_columns}, count(*) as samples, max({SAMPLE_TS}) - min({SAMPLE_TS}) as interval, period, date from ({grouped}) as grouped group by (period, date)'.format(**locals()))
        averaged = ['{pref}_{seconds}'.format(**locals()) for pref in categories]
        a = averaged[0]
        joined_tables =[str(a)]
        for b in averaged[1:]:
            joined_tables.append('inner join {b} on {a}.date = {b}.date and {a}.period = {b}.period'.format(a=a, b=b))
            a = b
        if set(categories) != {'mppt', 'pump'}:
            print('cannot create full averaging views since one of the categories is missing')
            return
        join_expr = ' '.join(joined_tables)
        cols = sum([cols for cols in dp_cols.values()], []) + ['pump_{}.date as date'.format(seconds), 'pump_{}.period as period'.format(seconds)]
        cols = ', '.join(cols)
        self.con.execute('create view avg_{seconds} as select {cols} from {join_expr}'.format(**locals()))


    def _create_views(self):
        for seconds in [1, 60, 5 * 60, 15 * 60]:
            self._create_views_for_averaging(seconds)

    def read_columns_and_create_tables(self, pref, csvfiles, timestamp_col, dtypes, converters):
        timestamp_col_sql = to_sql_column(timestamp_col)
        files = []
        columns = dict()
        #csvfiles = csvfiles[:10] # DEBUG
        n = len(csvfiles)
        for i, f in enumerate(csvfiles):
            print('{i_1} / {n}'.format(i_1=i + 1, n=n), file=stderr, end='\r')
            try:
                first = pd.read_csv(f, nrows=5)
            except Exception as e:
                faults['could not parse first 5 rows'].append(f)
                continue
            if timestamp_col not in first.columns:
                faults['missing timestamp_col'].append(f)
                continue
            first = pd.read_csv(f, nrows=5, parse_dates=[timestamp_col], dtype=dtypes)
            sql_dtypes = {to_sql_column(c): v for c, v in dtypes.items()}
            columns_and_types = {to_sql_column(c): first[c].dtype for c in first.columns}
            for col, dtype in columns_and_types.items():
                if col == timestamp_col_sql:
                    continue
                if col not in converters and col not in dtypes and is_float_dtype(dtype):
                    converters[col] = tryfloat(col)
                sql_converters = {to_sql_column(c): v for c, v in converters.items()}
                if col not in columns:
                    #if 'temp_ext' in col: b()
                    if col in sql_dtypes:
                        columns[col] = sql_dtypes[col]
                    elif col in sql_converters:
                        columns[col] = sql_converters[col]
                        if callable(columns[col]):
                            columns[col] = np.float
                    if col not in columns:
                        columns[col] = dtype #pandas_to_postgres_type.get(dtype)
            files.append(f)
        self.columns[pref] = columns
        if self.use_partition: # create the parent table
            self._create_table(pref, table_name=self._table_name(pref, None))
        return files

    def _table_name(self, pref, date):
        if self.use_partition:
            return str(pref)
        return '{pref}_{date}'.format(pref=pref, date=date)

    def _create_table(self, pref, table_name):
        columns = self.columns[pref]
        if len(columns) == 0:
            print("{}: no columns, skipping".format(table_name), file=stderr)
            return
        timestamp_column = '{SAMPLE_TS} timestamp'.format(SAMPLE_TS=SAMPLE_TS)  # no primary key nor unique constraints on partitioned table
        timestamp_col = self.category_to_timestamp_col[pref]
        sql_columns = ', '.join('{col} {type}'.format(col=col, type=pandas_to_postgres_type[dtype]) for col, dtype in columns.items() if col != timestamp_col)
        table_name = self._table_name(pref, None)
        if self.engine.has_table(table_name):
            self.drop(table_name)
        print("creating {}".format(table_name), file=stderr)
        create_statement = 'CREATE TABLE {table_name} ({timestamp_column}, {sql_columns})'.format(**locals())
        if self.use_partition:
            create_statement = '{create_statement} PARTITION BY RANGE ({SAMPLE_TS})'.format(**locals())
        self.con.execute(create_statement)
        return table_name

    def _create_partition(self, pref, start_date, end_date):
        date = start_date.strftime('%Y%m%d')
        table_name = self._table_name(pref, date)
        if not self.use_partition:
            return self._create_table(pref, table_name)
        if (pref, date) in self.created_partition:
            return table_name
        self.created_partition[(pref, date)] = True
        # add partition, see https://www.postgresql.org/docs/current/static/sql-createtable.html
        start_sql = start_date.strftime('%Y-%m-%d 00:00:00') # inclusive
        end_sql = end_date.strftime('%Y-%m-%d 00:00:00') # exclusive
        partition_name = '{pref}_{date}'.format(pref=pref, date=date)
        print("creating partition {}".format(partition_name), file=stderr)
        self.con.execute("create table {partition_name} partition of {table_name} for values from ('{start_sql}') to ('{end_sql}')".format(**locals()))
        return table_name

    def rebuild(self, logsroot, removefirst=True):
        """
        We create two tables, one for each pref, so one for mppt and one for pump.

        To allow for future changes to the CSV, we do not store the table structure.
        However, we do store a bit of it:
         - we explicitly store above any column which is a str. We can deduce it
           with a little more work however, in the future
         - the rest are assumed to be floating points. To fix

        Also, we assume all csv files have a single header column.

        The tables are created with partitions by day, to allow faster access.

        To generate the columns, we do a first pass on all the files, reading the starting lines
        (also used to throw some/all of the bad files).

        """
        if removefirst:
            self.removeall()

        con = self.con

        def filter_files(files):
            return files
            if 'testlogs/mppt_20180505_0000.csv' in files:
                return ['testlogs/mppt_20180505_0000.csv']
            return []

        # First pass: create tables, create views
        with timeit('create tables and views'):
            data = []
            for pref, dtypes, converters in [
                    ('mppt', mppt_dtypes, mppt_converters),
                    ('pump', pump_dtypes, {}),
                    ]:
                timestamp_col = self.category_to_timestamp_col[pref]
                files = glob(path.join(logsroot, '{pref}_????????_????.csv'.format(pref=pref)))
                files = filter_files(files)
                not_files = []
                files = sorted(set(files) - set(not_files))
                if len(files) == 0:
                    continue
                files = self.read_columns_and_create_tables(pref=pref, csvfiles=files, timestamp_col=timestamp_col, dtypes=dtypes, converters=converters)
                print("{pref}: {n} files".format(pref=pref, n=len(files)), file=stderr)
                data.append((pref, timestamp_col, files, dtypes, converters))
            self._create_views()

        # Second pass: populate tables
        for pref, timestamp_col, files, dtypes, converters in data:
            files = filter_files(files)
            with timeit(pref):
                for i_f, f in enumerate(files):
                    with timeit(f):
                        junk, date, morejunk = path.basename(f).split('_')
                        #if date != '20180425':
                        #    continue
                        for i, df in enumerate(
                                 breakcheck(lambda: pd.read_csv(null_cleaned_file(f), engine='python', index_col=timestamp_col,
                                             parse_dates=True,
                                             date_parser=lambda micros: dateparse(micros, tz=tz),
                                             error_bad_lines=False,
                                             dtype=dtypes, converters=converters, memory_map=True,
                                             chunksize=1024 # low_memory=True,
                                             ))):
                            # postgresql cannot handle DateTimeIndex so convert to python
                            # datetime
                            # see: https://stackoverflow.com/questions/38516251/valueerror-cannot-cast-datetimeindex-to-dtype-datetime64us#38530416
                            #df[timestamp_col] = pd.to_datetime(df[timestamp_col])
                            #df[timestamp_col].tz_localize(None)
                            df = df.reset_index().rename(columns = {timestamp_col: SAMPLE_TS})
                            df[SAMPLE_TS]= df[SAMPLE_TS].apply(lambda d: datetime.fromtimestamp(d.timestamp()))
                            df.columns = [to_sql_column(x) for x in df.columns]
                            df = df.set_index(SAMPLE_TS)
                            min_date = df.index.min()
                            max_date = df.index.max()
                            if df.shape[0] == 0 or pd.isna(min_date) or pd.isna(max_date):
                                faults['empty file'].append(f)
                                continue
                            print("{pref} {f} start-end: {min} {max}".format(pref=pref, f=f, min=df.index.min(), max=df.index.max()))
                            for start_date, end_date in partition_dates(df.index.min().date(), df.index.max().date() + timedelta(days=1)):
                                self._create_partition(pref, start_date, end_date)
                            table_name = self._table_name(pref, date)
                            df.to_sql(table_name, con, if_exists='append')
        with open('rebuild_results.pickle', 'wb+') as fd:
            pickle.dump([faults, convertions], fd)


def get_connection(user=None, password=None, host=None, db=None, port=None):
    loggedin = pwd.getpwuid(getuid())[0]
    if user is None:
        user = loggedin
    if password is None:
        password = loggedin
    if host is None:
        host = 'localhost'
    if db is None:
        db = loggedin
    if port is None:
        port = 5432
    return 'postgres://{user}:{password}@{host}:{port}/{db}'.format(**locals())


def get_connection_argparser():
    parser = argparse.ArgumentParser()
    parser.add_argument('--host', default=None)
    parser.add_argument('--user', default=None)
    parser.add_argument('--pass', dest='password', default=None)
    parser.add_argument('--db', default=None)
    parser.add_argument('--port', default=None, type=int)
    return parser


def argparser_connection(args=None):
    if args is None:
        args = get_connection_argparser().parse_args()
    return get_connection(user=args.user, password=args.password,
                          db=args.db, host=args.host, port=args.port)


def rebuild():
    parser = get_connection_argparser()
    parser.add_argument('--logs', required=True)
    args = parser.parse_args()
    sql = SQL(connection=argparser_connection(args))
    sql.rebuild(logsroot=args.logs)


if __name__ == '__main__':
    rebuild()
