#    pumpmonwebpanel - Comet-ME Water pump web monitoring
#    Copyright (C) 2018 Comet-ME
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""
Constants used by both client and server

The client gets them from the server via tojson Jinga filter
"""

from collections import OrderedDict
from itertools import chain


floating_point_digits_after_decimal_point = 1

PUMP_AVERAGE_POWER_VARNAME = 'controller.stats.cycle_power_running_average.average'


PUMP_CAT = 'pump'
MPPT_CAT = 'mppt'
FLOW_CAT = 'flow'

categories = [PUMP_CAT, FLOW_CAT, MPPT_CAT]

"""
Headers Whitelist

Only headers in here are shown and saved when downloading averaged files.
"""
headers_whitelist = {
    'averages': OrderedDict([
        ('FlowDaily', 1),
        ('FlowLatest', 1),
        ('PowerDaily', 1),
        ('PowerLatest', 1),
        ('LastMinute', 1),
    ]),
    MPPT_CAT: OrderedDict([
        ('RecordedTime', 1),
        ('Pi', 1),
        ('Po', 1),
        ('Tmp', 1),
        ('Flgs.OV', 1),
        ('Flgs.OT', 1),
        ('Flgs.TF', 1),
        ('Flgs.OFF', 1),
        ('Flgs.UV', 1),
    ]),
    PUMP_CAT: OrderedDict([
        ('timestamp', 1),
        ('controller.state.mode', 1),
        (PUMP_AVERAGE_POWER_VARNAME, 1),
        ('controller.stats.average_pressure', 1),
        ('controller.state.analog_sensors.dc_bus_v', 1),
        ('controller.stats.flow_rate_running_average.average', 1),
        ('controller.stats.cycle_current_running_average.average', 1),
        ('controller.stats.cycle_cruising_current_running_average.average', 1),
        ('controller.stats.cycle_peak_current_running_average.average', 1),
        ('controller.state.analog_sensors.temp_ext', 1),
        ('controller.state.analog_sensors.temp_a', 1),
        ('controller.state.analog_sensors.temp_b', 1),
        ('controller.state.analog_sensors.temp_c', 1),
    ]),
    FLOW_CAT: OrderedDict([
        ('time', 1),
        ('rate', 1),
    ]),
}

"""
Display headers. Used for both webpanel and downloaded CSV files.

If a name repeates the implicit action is *max*

If we ever want to add (avg, etc.) configurable actions, we can make it
explicit here.

Anything not in the mapping is implied unchanged (only '.' is converted to ' '
for word breaking in the displayed table).
"""
display_mapping = {
    'averages': {},
    MPPT_CAT: {
        'Pi': 'Power input',
        'Po': 'Power output',
    },
    PUMP_CAT: {
        'controller.state.analog_sensors.dc_bus_v': 'Pump input voltage',
        PUMP_AVERAGE_POWER_VARNAME: 'Pump average power',
        'controller.stats.average_pressure': 'Pump average pressure',
        'controller.stats.flow_rate_running_average.average': 'Averaged flow rate',
        'controller.stats.cycle_current_running_average.average': 'Pump cycle average current',
        'controller.stats.cycle_cruising_current_running_average.average': 'Pump cruising average current',
        'controller.stats.cycle_peak_current_running_average.average': 'Pump averaged peak current',
        'controller.state.analog_sensors.temp_ext': 'Motor temperature',
        'controller.state.analog_sensors.temp_a': 'Controller temperature',
        'controller.state.analog_sensors.temp_b': 'Controller temperature',
        'controller.state.analog_sensors.temp_c': 'Controller temperature',
        'controller.state.mode': 'Controller State',
    },
    FLOW_CAT: {
        'rate': 'Measured Flow LPM',
    },
}

value_mapping = {
    'averages': {},
    PUMP_CAT: {
        'controller.state.mode': {'map': {
            'MODE_UNDERVOLTAGE_SHUTDOWN': 'Under voltage',
            'MODE_ACCEL': 'Normal',
            'MODE_CRUISING': 'Normal',
            'MODE_DIR_CHANGE': 'Normal',
            'MODE_STALLED': 'Stalled',
            'MODE_INIT': 'Normal',
            'MODE_OPEN_LOOP': 'Normal',
            'MODE_MANUAL_OFF': 'Manual off',
            'MODE_SELF_OFF': 'Self off'
        }},
    },
    MPPT_CAT: {
    },
    FLOW_CAT: {
    },
}

# luckily the columns do not overlap
display_mapping_flat = {k: v for k, v in chain(*[x.items() for x in display_mapping.values()])}

"""
Do not show on the webpanel while saving in the merged averaged CSV file.

Note an exception: mppt.RecordedTime isn't because it is subsumed by
pump.timestamp
"""
webpanel_do_not_display = {
    'averages': {},
    MPPT_CAT: {
        'RecordedTime': 1,
    },
    PUMP_CAT: {
        'timestamp': 1,
    },
    FLOW_CAT: {
        'time': 1,
    }
}
