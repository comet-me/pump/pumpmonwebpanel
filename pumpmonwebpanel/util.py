from collections import OrderedDict


class OrderedDefaultDict(OrderedDict):

     def __init__(self, ctor):
         self.ctor = ctor

     def __getitem__(self, k):
         if k not in self.keys():
             self[k] = self.ctor()
         return super().__getitem__(k)


def max_or_none(l):
    if len(l) == 0:
        return None
    return max(l)

