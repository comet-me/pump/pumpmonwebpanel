#    pumpmonwebpanel - Comet-ME Water pump web monitoring
#    Copyright (C) 2018 Comet-ME
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import heapq
from glob import glob
from os import environ
from os.path import join, exists, basename
from os import stat as os_stat
from datetime import datetime, timedelta
import string
from collections import defaultdict


def now():
    return datetime.now()


MAIN_PREFIX = 'pump'
prefixes = list(sorted(['flow', 'mppt', MAIN_PREFIX]))
SUFFIX = '.csv'

ERROR_RESULT = {'error': 'nothing,found'}


def basename_to_timestamp(basename):
    """
    Given STRING_YYYYMMDD_HHMM.csv or STRING_YYYYMMDD.csv
    return datetime of date and time part
    """
    parts = basename[:-4].split('_')
    if len(parts) == 2 or (len(parts) >= 3 and len(parts[2]) < 4):
        yyyymmdd, hhmm = parts[1], '0000'
    elif len(parts) >= 3:
        yyyymmdd, hhmm = parts[1], parts[2]
    year, month, day = int(yyyymmdd[:4]), int(yyyymmdd[4:6]), int(yyyymmdd[6:8])
    hour, minute = int(hhmm[:2]), int(hhmm[2:4])
    return datetime(year=year, month=month, day=day, hour=hour, minute=minute)


class BackingOffStat:
    """
    implement a backing off stat checker

    For each file, maintain a last stat result, and next time to check
    If result missing or different then this one, reset next time to 0 (i.e. the next call)
    Otherwise, change to 2 or multiply if already >= 2, but max out at 60
    """
    initial_non_zero_seconds = 1
    min_change_expect_no_update = 0.1
    max_seconds = 32

    def __init__(self):
        self.last = defaultdict(lambda: (0, now(), None))

    def __call__(self, filename):
        timestamp = now()
        last_dt, last_timestamp, last_res = self.last[filename]
        dt = (timestamp - last_timestamp).seconds
        if last_res is not None:
            if dt < self.min_change_expect_no_update:
                return last_res # f'small dt {dt} < {self.min_change_expect_no_update}'
            if last_dt > dt:
                return last_res # f'old {last_dt} > {dt}'
        res = os_stat(filename)
        if res == last_res:
            # 'new (same)'
            dt = min(self.max_seconds, max(last_dt * 2, self.initial_non_zero_seconds))
        else:
            # 'new (diff)'
            dt = 0
        self.last[filename] = (dt, timestamp, res)
        return res


stat = backingoffstat = BackingOffStat()


def scalar_if_1(n, l):
    if n == 1:
        return l[0] if len(l) == 1 else None
    return l


def last_files(sources, prefixes, days=1):
    return {
        prefix:
            scalar_if_1(
                days,
                [f for f in sources
                            if basename(f).startswith(prefix)]
            )
        for prefix in prefixes
    }


cache = {} # {filename: (last seen offset, {header: last seen header, last: last line})}


def cached_offset(filename):
    data = cached_data(filename)
    return data[0] if data is not None else None


def cached_data(filename):
    if filename not in cache:
        return None
    return cache[filename]


def cached_key(filename, key):
    data = cached_data(filename)
    if data is None or key not in data[1]:
        return None
    return data[1][key]


def cached_last(filename):
    return cached_key(filename, 'last')


def cached_header(filename):
    if filename not in cache:
        cache_initialize(filename)
    return cached_key(filename, 'header')


letters = set(string.ascii_letters)


def csv_parse(line):
    # NB: lots of CSV magic we are ignoring - no spaces in variables and enum values, we don't need to deal with quotes
    try:
        return [x.strip() for x in line.split(',')]
    except:
        return []


def cache_update(filename, offset, last):
    csv_line = csv_parse(last)
    if filename in cache:
        d = cache[filename][1]
    else:
        d = {}
    cache[filename] = (offset, d)
    d['last'] = csv_line
    if len(csv_line) > 0 and len(csv_line[0]) > 0 and csv_line[0][0] in letters:
        d['header'] = csv_line
    return csv_line


def cache_initialize(filename):
    if filename in cache:
        return
    # store the last headers from the file
    with open(filename, 'rb') as fd:
        for line in fd.readlines():
            line = line.strip().decode('utf-8')
            if len(line) == 0:
                continue
            cache_update(filename, fd.tell(), line)


def last_line(filename):
    """
    Return the last line of the filename which is a CSV file.

    Optimization strategies:
     - read only from the last offset
     - save the header if the line is a header (done by cache_update)

    TODO:

     read all last lines

     check it cannot miss any (offset always increasing, if stuck in middle of line
     it will be returned once it is covered, even if in the middle another half a
     line is read)

     at caller parse all the lines: specifically indexer (now the only client - should be)

     remove other clients (main.py), leave only indexer
    """
    size = stat(filename).st_size
    cache_initialize(filename)
    if filename in cache and cache[filename][0] == size:
        # no update
        return cached_last(filename)
    last_pos = cached_offset(filename)
    with open(filename, 'rb') as fd:
        #print("DEBUG: {last_pos}")
        if last_pos is not None:
            fd.seek(last_pos)
        txt = fd.read().decode('utf-8')
        res = [x.strip() for x in txt.rsplit('\n', 1) if x.strip() != '']
        if len(res) == 0:
            return cached_last(filename)
        line = res[-1]
        if '\n' in line:
            line = line.split('\n')[0]
        return cache_update(filename, fd.tell(), last=line)
    raise Exception()
    return None


def lastheader(filename):
    return cached_header(filename)


def lastdatum(filename):
    if filename is None:
        return {}
    header = lastheader(filename)
    values = last_line(filename)
    if header is None or values is None:
        return [('error header' if header is None else 'header ok', 'error values' if values is None else 'values ok')]
    if len(values) < len(header):
        values = values + [None] * (len(header) - len(values))
    return list(zip(header, values))


LOGS_PATH_ENV = 'PUMPMON_LOGS'


def logs_path():
    return environ.get(LOGS_PATH_ENV, '.')


def current_logspath():
    logs = logs_path()
    # TODO: can make this cheaper? all the calls to the time should
    # be moved to a 'loop' object and just cached
    now = datetime.now()
    year = '{:04}'.format(now.year)
    month = '{:02}'.format(now.month)
    day = '{:02}'.format(now.day)
    return join(logs, year, month, day)


def current_logspaths(n):
    """
    return list of directories for last n days, n=1 means just today
    """
    logs = logs_path()
    now = datetime.now()
    ret = []
    for i in range(n):
        d = now - timedelta(days=i)
        year = '{:04}'.format(d.year)
        month = '{:02}'.format(d.month)
        day = '{:02}'.format(d.day)
        ret.append(join(logs, year, month, day))
    return ret


def last_files_of_logspath(n=1):
    logs = current_logspaths(n)
    logs = [l for l in logs if exists(l)]
    if len(logs) == 0:
        return ERROR_RESULT
    files = sum([glob(join(l, '*' + SUFFIX)) for l in logs], [])
    return last_files(files, prefixes, n)


def raw_last_data():
    lastfs = last_files_of_logspath()
    if lastfs is ERROR_RESULT:
        return {}
    return {prefix: lastdatum(f) for prefix, f in lastfs.items()}


if __name__ == '__main__':
    if 'PUMPMON_LOGS' not in environ:
        environ['PUMPMON_LOGS'] = '.'
    d = last_files_of_logspath()
    if True:
        print(d)
        print('-' * 30)
        for prefix, filename in d.items():
            print(last_line(filename))
        print('-' * 30)
        for prefix, filename in d.items():
            print(lastheader(filename))
    data =  last_data()
    for prefix, data in data.items():
        print("{}: {}".format(prefix, data))

