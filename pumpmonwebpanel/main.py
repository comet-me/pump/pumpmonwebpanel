#!/bin/env python3

#    pumpmonwebpanel - Comet-ME Water pump web monitoring
#    Copyright (C) 2018 Comet-ME
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

from pdb import set_trace as b
from sys import stderr
from datetime import datetime, date
import string
from os import path, system, environ, unlink
import shutil
import base64
from itertools import chain
from subprocess import check_output, check_call, CalledProcessError, STDOUT
from tempfile import TemporaryDirectory
from contextlib import contextmanager
import traceback


from flask import (
    Flask, render_template, jsonify, send_from_directory, Response, request, json,
    url_for)
try:
    import usb
    no_usb = False
except:
    no_usb =True

from .util import OrderedDefaultDict
from .logs import logs_path, last_files_of_logspath
from .indexer import last_data, last_averages_per_minute
from .consts import (
    headers_whitelist, display_mapping, webpanel_do_not_display,
    floating_point_digits_after_decimal_point, value_mapping,
    categories
)
from .average_old import average as average_old
from .average import average, celery_average


from . import VERSION


# Fail early if missing environment variables
PUMPMON_VARS = environ['PUMPMON_VARS']
PUMPMON_ELF = environ['PUMPMON_ELF']


app = Flask(__name__,
        static_folder='static',
        template_folder='templates')

webapp = app # NOTE: for gunicorn - remove when systemd is updated


leggible_chars = set(string.printable)


def print_last():
    try:
        traceback.print_last()
    except:
        traceback.print_stack(file=stderr)


def leggible(st):
    if st is None:
        return 'None'
    if not isinstance(st, str):
        st = str(st)
    return st if set(st) <= leggible_chars else 'illegible (baudrate?)'


def latest_helper():
    last = last_data()
    data = {
        prefix: [(leggible(k), leggible(v)) for k, v in data] if data is not None else 'Nothing'
        for prefix, data in last.items()
    }
    return data


def parse_command_line(cmdline):
    ret = {}
    parts = cmdline.split()
    flag = None
    for p in parts:
        if p.startswith('--'):
            flag = p[2:]
        else:
            if flag is not None:
                ret[flag] = p
    return ret


def record_mppt_write_env(print_time=None, loop_time=None):
    """
    Read environment file of record_mppt and rewrite or add an RECORD_MPPT_EXTRA line
    with given print_time and loop_time.

    Most code works to keep the previously defined times so that a change only changes
    the given parameter. A 'None' means no change to that parameter.
    """
    env = '/etc/pumpmon/record_mppt.env'
    # ugly, but guranteed to not explode and not lose data
    with open(env) as fd:
        lines = fd.readlines()
    # if there is a #WEBPANEL line than just keep the #WEBPANEL prefixed lines
    # otherwise comment everything and add a RECORD_MPPT line
    optional_existing_line = [x for x in lines if x.strip().startswith('RECORD_MPPT_EXTRA')]
    if len(optional_existing_line) > 0:
        existing_record_mppt_extra = optional_existing_line[-1]
        cmdline = parse_command_line(existing_record_mppt_extra)
        if print_time is not None and 'print_time' in cmdline:
            print_time = cmdline['printmilli']
        if loop_time is not None and 'loop_time' in cmdline:
            loop_time = cmdline['loopmilli']
    if any([x.strip().startswith('#WEBPANEL') for x in lines]):
        kept = [l for l in lines if l.strip().startswith('#WEBPANEL')]
    else:
        kept = ['#WEBPANEL ' + l for l in lines]
    newlines = ['RECORD_MPPT_EXTRA=--printmilli {print_time} --loopmilli {loop_time}\n'.format(print_time=print_time, loop_time=loop_time)] + kept
    with open(env, 'w+') as fd:
        fd.writelines(newlines)


@app.route("/restart/mppt_loop_time/<int:loop_time>")
def restart_mppt_loop_time(loop_time):
    restart_mppt(loop_time=loop_time)
    return 'ok'


@app.route("/restart/print_time/<int:print_time>")
def restart_print_time(print_time):
    restart_both_with_print_time(print_time)
    return 'ok'


def restart_both_with_print_time(print_time):
    """
    Change time constants for both mppt and print_time
    Restart record_mppt and record_controller
    """
    restart_mppt(print_time=print_time)
    restart_controller(print_time)


def restart_mppt(print_time=None, loop_time=None):
    record_mppt_write_env(print_time=print_time, loop_time=loop_time)
    system("systemctl restart record_mppt")


def restart_controller(print_time):
    # TODO - do this via a variable instead of rewriting every line - which could lead to
    # data loss due to rounding errors
    if not path.exists(PUMPMON_VARS):
        print('error: missing vars file {}'.format(PUMPMON_VARS))
        return
    varsfile_dir = path.dirname(PUMPMON_VARS)
    for i in range(10000):
        backup = path.join(varsfile_dir, 'vars.{}.bak'.format(i))
        if not path.exists(backup):
            shutil.copy(varsfile, backup)
            break
    with open(varsfile) as fd:
        lines = fd.readlines()
    out = []
    # hardcode for now: 20000 is 1 second
    # TODO - should be taken from ELF file ultimately, source of knowledge.
    ticks_per_second = 20000
    ticks = str(int(print_time / 1000.0 * ticks_per_second))
    for line in lines:
        if line.strip()[:1] == '#':
            out.append(line)
        else:
            parts = line.split(',')
            out.append(','.join(parts[:1] + [ticks] + parts[2:]))
    with open(varsfile, 'w+') as fd:
        fd.writelines(out)
    system("systemctl restart record_controller")


@app.route("/latest")
def latest():
    client_version = request.args.get('client_version')
    data = latest_helper()
    if client_version:
        ret = dict(data=data, meta=dict(version=VERSION), server_state=get_server_state())
    else:
        ret = data
    return jsonify(ret)


def relative_last_files_of_logspath():
    files = last_files_of_logspath()
    logs = logs_path()
    return [path.relpath(f, start=logs) for f in files.values() if f is not None]


@app.route("/files")
def files():
    return jsonify(relative_last_files_of_logspath())


@app.route("/download/<path:path>")
def download(path):
    return send_from_directory(logs_path(), path)


def parse_dashed_date(s, hour, minute):
    s_y, s_m, s_d = map(int, s.split('-'))
    return datetime(year=s_y, month=s_m, day=s_d, hour=hour, minute=minute)


def average_root():
    return path.join(logs_path(), 'average')


def average_result_filename(start_date, end_date, interval, remove_under_voltage):
    remove_under_voltage = 'uv0' if remove_under_voltage else 'all'
    return "pump__{TITLE_BLURB}__{start}__{end}__{interval}__{remove_under_voltage}.csv".format(
        start=start_date.strftime('%Y%m%d'),
        end=end_date.strftime('%Y%m%d'),
        interval=interval,
        TITLE_BLURB=TITLE_BLURB,
        remove_under_voltage=remove_under_voltage)


# TODO - remove GET? not RESTfull (but makes debugging easier for firefox POST
# addon challenged me)
@app.route("/task/start/avg/<int:interval>/<start_date>/<end_date>/")
def start_download_averaged_old(interval, start_date, end_date):
    return start_download_averaged_helper(
        interval=interval,
        start_date=start_date,
        end_date=end_date)


@app.route("/task/start/avg/<int:interval>/<start_date>/<end_date>/<string:remove_under_voltage>/")
def start_download_averaged_new(interval, start_date, end_date, remove_under_voltage):
    remove_under_voltage = remove_under_voltage.lower()[:1] in {'y', '1'}
    return start_download_averaged_helper(
        interval=interval,
        start_date=start_date,
        end_date=end_date,
        remove_under_voltage=remove_under_voltage,
    )


def start_download_averaged_helper(interval, start_date, end_date, remove_under_voltage=True):
    """ only supports new style """
    start_date_dt, end_date_dt = parse_dashed_date(start_date, 0, 0), parse_dashed_date(end_date, 23, 59)
    basename = average_result_filename(start_date_dt, end_date_dt, interval, remove_under_voltage)
    target_filename = path.join(average_root(), basename)
    # TODO: erase old files for now. Perhaps we can cache them, but have to do it
    # carefully - making sure they actually contain the same contents always
    if path.exists(target_filename):
        unlink(target_filename)
    celery_average.delay(
        start_date=start_date_dt.replace(hour=0, minute=0, second=0, microsecond=0),
        end_date=end_date_dt.replace(hour=23, minute=59, second=59, microsecond=999999),
        period_seconds=float(interval),
        root=logs_path(),
        headers_whitelist=headers_whitelist,
        target_filename=target_filename,
        remove_under_voltage=remove_under_voltage,
    )
    url = url_for('status_download_averaged', basename=basename)
    return jsonify({"status_url": url})


@app.route("/task/status/avg/<basename>")
def status_download_averaged(basename):
    fullname = path.join(average_root(), basename)
    if not path.exists(fullname):
        return jsonify({'status': 'pending'})
    # Return a status indicating client should query for the actual response
    result_url = url_for('result_download_averaged', basename=basename)
    return jsonify({'status': 'result', 'result_url': result_url})


@app.route("/task/result/avg/<basename>")
def result_download_averaged(basename):
    return send_from_directory(average_root(), basename)


def dfgen(df):
    yield df.index.name + ',' + ','.join(df.columns) + '\n'
    for i, row in df.iterrows():
        yield str(i) + ',' + ','.join(list(map(str, row))) + '\n'


@app.route("/today/<int:interval>/")
@app.route("/avg/<int:interval>/")
@app.route("/averaged/<int:interval>/")
@app.route("/download_averaged/<int:interval>")
def download_averaged(interval):
    """
    Note: interval is in minutes, average takes seconds (TODO: rename parameter)
    """
    use_old = request.args.get('use_old')
    if request.url.split('://')[1].split('/')[1] == 'today':
        now = datetime.now()
        start_date = end_date = now
    else:
        start_date_str = request.args.get('start')
        end_date_str = request.args.get('end')
        s_y, s_m, s_d = map(int, start_date_str.split('-'))
        e_y, e_m, e_d = map(int, end_date_str.split('-'))
        start_date = datetime(year=s_y, month=s_m, day=s_d, hour=0)
        end_date = datetime(year=e_y, month=e_m, day=e_d, hour=23, minute=59)
    # print("got {start_date} {end_date} {interval}")
    if use_old:
        csv = average_old(start_date=start_date.date(), end_date=end_date.date(),
                      period=float(interval) * 1000, path=logs_path(),
                      headers_whitelist=headers_whitelist)
    else:
        df = average(
            start_date=start_date.replace(hour=0, minute=0, second=0, microsecond=0),
            end_date=end_date.replace(hour=23, minute=59, second=59, microsecond=999999),
            period_seconds=float(interval), root=logs_path(),
            headers_whitelist=headers_whitelist,
        )
        csv = dfgen(df)
    return Response(csv, mimetype='text/csv', headers=[("Content-Disposition",
        "attachment;filename=pump__{}__{end}__{interval}.csv".format(
            start=start_date.strftime('%Y%m%d'),
            end=end_date.strftime('%Y%m%d'),
            interval=interval,
        ))])


@app.route("/version")
def version():
    return str(VERSION)


def read_vars_file():
    try:
        with open(PUMPMON_VARS, 'r') as fd:
            return fd.read()
    except:
        return 'missing or unreadable vars file'


@app.route("/varscsv", methods=['GET', 'POST'])
def varscsv():
    if request.method == 'GET':
        return jsonify(read_vars_file())
    # POST
    vars_txt = json.loads(request.form['json'])
    ok, error = verify_emolog_executable_and_vars(elf_path=PUMPMON_ELF, vars=vars_txt)
    if ok:
        try:
            with replace_vars_file(vars_txt):
                pass
        except Exception as e:
            print_last()
            ok, error = False, str(e)
    restart_record_controller()
    return jsonify(dict(ok=ok, error=error))


def contents_to_file(contents, filename):
    if isinstance(contents, bytes):
        mode = 'wb+'
    else:
        mode = 'w+'
    with open(filename, mode) as fd:
        fd.write(contents)
    return filename


def verify_emolog_executable_and_vars(elf=None, elf_path=None, vars=None, vars_path=None):
    """
    Uses the existing vars file based on environment variable and checks
    that the ELF contains those variables, and is in fact a valid ELF.

    TODO: verify it is the right target arch
    """
    ok, error = True, None

    with TemporaryDirectory() as temp_dir:
        if elf_path is None:
            assert elf is not None
            elf_path = contents_to_file(elf, path.join(temp_dir, 'candidate.out'))
        if vars_path is None:
            assert vars is not None
            vars_path = contents_to_file(vars, path.join(temp_dir, 'candidate.out'))
        assert vars_path is not None
        assert elf_path is not None
        try:
            output = check_output(
                ['emotool-dwarf', '-v', vars_path, '-e', elf_path],
                stderr=STDOUT).decode()
        except CalledProcessError as e:
            print_last()
            output = e.output.decode()
            # make some output more human readable
            if 'elftools.common.exceptions.ELFError' in output:
                # TODO: this is a workaround around a bug in emotool-dwarf - it
                # should catch the error and print a human readable error like
                # this.
                output = 'file is not a valid ELF file'
            ok, error = False, output
        except Exception as e:
            print_last()
            ok, error = False, 'general failure: {}'.format(e)
        else:
            if output.strip() != 'ok':
                ok, error = False, output
    return ok, error


def restart_record_controller():
    service = 'record_controller.service'
    check_call(['systemctl', 'restart', service])
    show = {x: check_output(['systemctl', 'show', '-p', x, service]).decode().split('=')[1].strip() for x in ('ActiveState', 'SubState')}
    ok_show = {"ActiveState": "active", "SubState": "running"}
    if ok_show != show:
        status = check_output(['systemctl', 'status', service]).decode()
        raise Exception("record_controller restart failed:\nshow:\n" + repr(show) + '\n\nstatus:\n' + status)


def backup_context_manager(filename, binary):
    """
    Context manager creator that stores filename aside as <filename>.old on success,
    and on failure reverts the change.

    Usage:
    my_file_backed_up = backup_context_manager(filename, binary=is_the_content_binary)
    with my_file_backed_up('file_contents_as_string' / b'file_contents_as_binary'):
        if everything_is_not_fine:
            raise Exception('to cause revertion to old file')
    """
    # TODO - use a wrapper?
    @contextmanager
    def backup_context_manager(contents):
        # TODO - don't replace old backup until verification. use TemporaryDirectory
        backup = filename + '.old'
        backedup = False
        if path.exists(filename):
            shutil.move(filename, backup)
            backedup = True
        with open(filename, 'wb+' if binary else 'w+') as fd:
            fd.write(contents)
        try:
            yield
        except Exception as e:
            print_last()
            if backedup:
                shutil.move(backup, filename)
            raise e
    return backup_context_manager


write_new_executable = backup_context_manager(PUMPMON_ELF, binary=True)
replace_vars_file = backup_context_manager(PUMPMON_VARS, binary=False)


 # TODO - put this string in constants (but different than the user facing
 # constants - those should be editable at some point by users)
REQUEST_POST_FILE_FIELD = 'file'


@app.route("/executable", methods=["POST"]) # no GET - we can only do best effort (until we have VERSION handshake)
def executable_update():
    elf_file_storage = request.files[REQUEST_POST_FILE_FIELD]
    elf = elf_file_storage.stream.read()
    ok, error = verify_emolog_executable_and_vars(elf=elf, vars_path=PUMPMON_VARS)
    if ok:
        try:
            with write_new_executable(elf):
                pass
        except Exception as e:
            print_last()
            ok, error = False, repr(e)
    # Lastly, asynchronously, restart record_controller
    restart_record_controller()
    return jsonify(dict(ok=ok, error=error)) # XXX - use 400? i.e. status(ok)
    # verify file - update emolog to learn to do this verification from command
    # line util, and do it fast (postpone uploads, or use a special console
    # tool)
    # return error (400) with string, client needs to handle as error (on Promise)
    # on correct file:
    # write file to location given by environment variable
    # variable set by deb package via systemd env file /etc/pumpmon/webpanel.env
    # than restart record_controller service


def get_http_headers():
    """
    headers to be used by client to connect back here.
    For setups with Basic Authorization used by an intermediate
    nginx server there is no way to discover it purely on the client side (or it
    is too much work) so provide it
    """
    ret = {}
    SERVER_BASIC_AUTHORIZATION_USER='SERVER_BASIC_AUTHORIZATION_USER'
    SERVER_BASIC_AUTHORIZATION_PASSWORD='SERVER_BASIC_AUTHORIZATION_PASSWORD'
    user = environ.get(SERVER_BASIC_AUTHORIZATION_USER, None)
    password = environ.get(SERVER_BASIC_AUTHORIZATION_PASSWORD, None)
    if user is not None and password is not None:
        enc = base64.encodebytes('{}:{}'.format(user, password).encode()).decode()
        ret['Authorization'] = 'Basic {}'.format(enc)
    return ret


def to_float(s, default=None):
    try:
        return float(s)
    except:
        return default


def is_float(x):
    try:
        float(x)
        return True
    except:
        return False


float_str_format = '{:.%sf}' % floating_point_digits_after_decimal_point


def get_display_data(data):
    """
    Logic replicated in client as well (root.html) but required to keep
    working for no javascript clients (and badly configured gateways)
    """
    pre_max = {}
    ret = {}
    for category, datum_pairs in data.items():
        d = pre_max[category] = OrderedDefaultDict(list)
        m = display_mapping.get(category, {})
        vm = value_mapping.get(category, {})
        wl = headers_whitelist[category]
        dnd = webpanel_do_not_display[category]
        datum = dict(datum_pairs)
        for var in wl.keys(): # order by headers_whitelist[category]
            if var in dnd or var not in datum.keys():
                continue
            value = datum[var]
            display = m.get(var, var)
            if var in vm and 'map' in vm[var]:
                value = vm[var]['map'].get(value, value)
            d[display].append(value)
    def pretty(v):
        if is_float(v):
            return float_str_format.format(max(0, float(v)))
        return v
    ret = {cat: [(k2, pretty(max(v))) for k2, v in d.items()] for cat, d in pre_max.items()}
    return ret


# (product, vendor) => name ('pump', 'mppt')
usb_devices = {
    (0x0403, 0x6010): 'pump',
    (0x0403, 0x6015): 'mppt',
}

def env_is_true(name):
    val = environ.get(name, None)
    return val is not None and val.lower() in ['1', 't', 'true', 'yes', 'y']

no_usb = no_usb or env_is_true('PUMPMON_NO_USB')

def read_usb_devices():
    ret = {k: False for k in usb_devices.values()}
    if no_usb:
        return ret
    for bus in usb.busses():
        for device in bus.devices:
            pair = (device.idVendor, device.idProduct)
            if pair in usb_devices:
                ret[usb_devices[pair]] = True
    return ret


usb_not_working = False

def get_server_state_no_usb():
    return dict(devices={k: False for k in usb_devices.values()}) # TODO: pass a state saying 'no access to usb' instead of 'usb not connected'


def get_server_state():
    """
    return which USB devices are available.
    return which services are up (that we care for)
    """
    # TODO: avoid starting processes. yes, it is cheap. how cheap? at least
    # benchmark it. <1 ms is the goal.
    #
    # times for the pi (for the i7 devel laptop)
    #  udevadm info --export-db > /dev/null : 0.3 seconds (0.03)
    #  lsusb > /dev/null: 0.5 (<0.01)
    #  this function with read_usb_devices (timeit via ipython: < 1microsecond
    global usb_not_working
    if usb_not_working:
        return get_server_state_no_usb()
    try:
        return dict(devices=read_usb_devices())
    except:
        usb_not_working = True
        return get_server_state_no_usb()


TITLE_BLURB = environ.get('PUMPMON_PANEL_SLUG', 'unconfigured')


@app.route("/")
def root():
    data = latest_helper()
    display_data = get_display_data(data)
    averages_data = dict(display_data['averages'])
    averages_data['per_minute'] = last_averages_per_minute()
    server_state = get_server_state()
    utcnow = datetime.utcnow()
    mppt_t = to_float(dict(data.get('mppt', {})).get('RecordedTime', None))
    pump_t = to_float(dict(data.get('pump', {})).get('timestamp', None))
    timestamp = max([0.0] + [x for x in (mppt_t, pump_t) if x is not None])

    http_headers = get_http_headers()

    return render_template(
        'root.html',
        display_data=display_data,
        averages_data=averages_data,
        rate=1000, # milliseconds
        refresh=0,
        files=relative_last_files_of_logspath(),
        timestamp=timestamp,
        timestamp_str=str(utcnow),

        # constants - avoids having a javascript and a separate RT.
        categories=categories,
        server_state=server_state,
        headers_whitelist=headers_whitelist,
        http_headers=http_headers,
        do_not_display=webpanel_do_not_display,
        display_mapping=display_mapping,
        value_mapping=value_mapping,
        version='.'.join(map(str, VERSION)),
        floating_point_digits_after_decimal_point=floating_point_digits_after_decimal_point,
        title_blurb=TITLE_BLURB,
        PUMPMON_ELF_basename=path.basename(PUMPMON_ELF),
        graph_width=128,
        graph_height=17,
        graph_enabled=False,
        )


def main():
    app.run(port=4000)


# Running externally with flask - this is in case it is run as a script
if __name__ == '__main__':
    main()

