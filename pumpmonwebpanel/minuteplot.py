#!/usr/bin/env python3

#    pumpmonwebpanel - Comet-ME Water pump web monitoring
#    Copyright (C) 2018 Comet-ME
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import re
from os import system, path, environ
import subprocess

from .sql import (
    SQL, get_connection_argparser, argparser_connection
)
import matplotlib.pyplot as plt

date_re = re.compile('^\d\d\d\d-\d\d-\d\d$')


def output_svg(args, points):
    plt.scatter(*list(zip(*points)), s=2)
    start_str = args.start.replace('-', '_')
    end_str = args.end.replace('-', '_')
    filename = 'daily_minute_{start_str}__{end_str}.svg'.format(start_str=start_str, end_str=end_str)
    plt.savefig(filename)
    return filename


def output_ascii(args, points):
    gnuplot = subprocess.Popen([gnuplot_exec],
                               stdin=subprocess.PIPE)
    gnuplot.stdin.write(b"set term dumb 79 25\n")
    gnuplot.stdin.write(b"plot '-' using 1:2 title 'Line1' with linespoints \n")
    for i,j in points:
       gnuplot.stdin.write(b"%f %f\n" % (i, j))
    gnuplot.stdin.write(b"e\n")
    gnuplot.stdin.flush()


def which(filename):
    for p in environ['PATH'].split(':'):
        candidate = path.join(p, filename)
        if path.exists(candidate):
            return candidate
    return None


gnuplot_exec = which('gnuplot')


output_functions = {
    'svg': output_svg,
}


if gnuplot_exec is not None:
    output_functions['ascii'] = output_ascii


def main():
    parser = get_connection_argparser()
    parser.add_argument('--start', help='YYYY-MM-DD')
    parser.add_argument('--end', help='YYYY-MM-DD')
    parser.add_argument('--output', choices=output_functions.keys())
    parser.add_argument('--open', action='store_true', default=False)
    parser.add_argument('--column', help='use ? to get list of columns (or any wrong column)', default='vo')
    args = parser.parse_args()
    if date_re.match(args.start) is None:
        print('bad start date, must be YYYY-MM-DD')
        return
    if date_re.match(args.end) is None:
        print('bad end date, must be YYYY-MM-DD')
        return
    connection = argparser_connection(args)
    print("connecting to {}".format(connection))
    sql = SQL(connection=connection)
    # get columns first, check allowed columns for '--column ?'
    columns = [r[0] for r in sql.con.execute("select column_name from information_schema.columns where table_name = 'mppt_60'")]
    if args.column == '?' or args.column not in columns:
        columns_str = '\n'.join(columns)
        print('possible columns (--column):\n{}'.format(columns_str))
        raise SystemExit
    query = "select period, {col} from mppt_60 where date >= '{start}' and date <= '{end}'".format(col=args.column, start=args.start, end=args.end)
    points = list(sql.con.execute(query))
    filename = output_functions[args.output](args, points)
    if filename and args.open:
        system('xdg-open {}'.format(filename))


if __name__ == '__main__':
    main()
