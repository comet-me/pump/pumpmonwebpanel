#!/usr/bin/env python3

#    pumpmonwebpanel - Comet-ME Water pump web monitoring
#    Copyright (C) 2018 Comet-ME
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import os
from sys import stderr
from glob import glob
from datetime import date, datetime, timedelta
from collections import defaultdict
from math import nan
import csv

from tzlocal import get_localzone

from .consts import display_mapping_flat

# TODO - use logging
debug = False


def _parse_filename(f):
    """
    Helper for get_files_in_range
    """
    filename = os.path.splitext(os.path.basename(f))[0]
    parts = filename.split('_')
    assert len(parts) >= 2, 'not enough filename parts - {}, {}, {}'.format(f, filename, parts)
    file_type, file_date = parts[:2]
    assert(len(file_date) == 8), "file_date is not 8 bytes: {}".format(repr(file_date))
    year_str, month_str, day_str = file_date[:4], file_date[4:6], file_date[6:8]
    d = date(year=int(year_str), month=int(month_str), day=int(day_str))
    return d, file_type


# NOTE: These must be the prefixes used in the saved files (i.e. currently
# identity of the X_FILE_TYPE and the X prefix is assumed)
PUMP_FILE_TYPE = 'pump'
MPPT_FILE_TYPE = 'mppt'

# This is the file_type used for STRATEGY_CLOSEST_TO_0 as the time source.
# See doc string of <merge_events> below
MAIN_FILE_TYPE = PUMP_FILE_TYPE

# For each new file_type, add the header for the timestamp. Timestamp is
# milliseconds since epoch.
TIME_HEADERS = {
    PUMP_FILE_TYPE: 'timestamp',
    MPPT_FILE_TYPE: 'RecordedTime'
}


def get_files_in_range(start_date, end_date, path):
    """
    Reads csv files from path, and based on the encoded date in the path
    returns a list of tuples
     (file_type, [(date, filename)])

    The list is sorted by date from first to last

    The date is encoded in the filename as:

    FILETYPE_YYYYMMDD[_C*].csv

    Where C is a consecutive number, lowest is earliest
    """
    all_filenames = glob(os.path.join(path, '*.csv'))
    ret_dicts = defaultdict(list)
    main = []
    for filename in all_filenames:
        d, file_type = _parse_filename(os.path.basename(filename))
        if d < start_date or d > end_date:
            continue
        if file_type == MAIN_FILE_TYPE:
            main.append((d, filename))
        else:
            ret_dicts[file_type].append((d, filename))

    rest = [(file_type, list(sorted(l))) for file_type, l in ret_dicts.items()]
    return [(MAIN_FILE_TYPE, list(sorted(main)))] + rest


def time_index_from_header(file_type, header):
    assert file_type in TIME_HEADERS
    ret = header.index(TIME_HEADERS[file_type]) # will raise ValueError (which is good) if not there
    return ret


def drop_columns_from_rows(rows, indices):
    # columnize events and drop there, then uncolumnize
    as_cols = list(zip(*rows))
    for rem_index in reversed(indices):
        del as_cols[rem_index]
    return list(zip(*as_cols))


class Single:
    def __init__(self, filename, header, events, headers_whitelist):
        self.filename = filename
        assert isinstance(header, list)
        assert isinstance(events, list)
        assert all(isinstance(event, list) for event in events)
        # sanity check: all events have the same length
        events_lengths = set(len(e) for e in events)
        assert len(events_lengths) == 1, 'non rectangular Single {}, lengths {}'.format(filename, events_lengths)
        # filter here if a filter is provided
        if headers_whitelist is not None:
            drop_indices = [i for i, h in enumerate(header) if h not in headers_whitelist]
            new_header = [h for h in header if h in headers_whitelist]
            if debug: print("debug: reducing columns per filter (|filter|={nw} from {nh} to {nnh}".format(
                    nw=len(headers_whitelist),
                    nh=len(header),
                    nnh=len(new_header),
                ), file=stderr)
            header = new_header
            events = drop_columns_from_rows(events, drop_indices)
        assert(len(header) >= 1)
        self.header = header
        self.events = events

    def __str__(self):
        return '<Single {} {}>'.format(self.filename, repr(self.header))

    def __repr__(self):
        return '<Single {} {}>'.format(self.filename, repr(self.header))


# TODO: this is a hack, a line can start with an enumeration and in that case it
# will not necessarily fit this test for headerness. Switch to having one header
# per CSV, which fixes this ambiguity
header_permitted_first_chars = set('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ')


def hist(l, f):
    ret = defaultdict(int)
    m = {}
    for i, item in enumerate(l):
        ret[f(item)] += 1
        m[f(item)] = item
    return ret, m


def break_file_to_singles(filename, file_type, headers_whitelist):
    """
    generator for each batch that is rectangular - i.e.
    each file_type has the same number of columns
    """
    with open(filename) as fd:
        lines = fd.readlines()

    ret = []
    events = []
    header = None

    def append(header, events, last_row_index):
        time_index = time_index_from_header(file_type=file_type, header=header)
        normalized_header_full = normalize_event(event=header, time_index=time_index, check_float=False)
        num_events = len(events)
        if num_events == 0:
            print('warn: empty file before normalization', file=stderr)
            return
        normalized_events_and_indices = [normalize_event(event=event, time_index=time_index, check_float=True) for event in events if len(event) == len(normalized_header_full)]
        num_normalized = len(normalized_events_and_indices)
        if num_events != num_normalized:
            print('warn: dropped {} during normalization due to not enough elements in row ({} needed)'.format(
                num_events - num_normalized, len(normalized_header_full)), file=stderr)
        if num_normalized == 0:
            print('warn: empty file after normalization', file=stderr)
            return
        normalized_events = [es for _, es in normalized_events_and_indices]
        normalized_indices = set(tuple(indices) for indices, _ in normalized_events_and_indices)
        if len(normalized_indices) != 1:
            # in case of inconsistencies, keep the biggest piece
            temp_d, temp_map = hist(normalized_events_and_indices, lambda indices_es: len(indices_es[0]))
            inv_temp_d = {v: k for k, v in temp_d.items()}
            _len = inv_temp_d[max(temp_d.values())]
            # drop all events that do not match
            dropped_indices = [i for i, ev in enumerate(normalized_events) if len(ev) != _len]
            print("warn: dropping part of single of {filename}: {dropped_indices} (last line index = {last_row_index})".format(
                filename=filename,
                dropped_indices=repr(dropped_indices),
                last_row_index=last_row_index,
            ), file=stderr)
            for i in reversed(dropped_indices):
                del normalized_events[i]
            normalized_index = temp_map[_len][0]
        else:
            normalized_index = list(normalized_indices)[0]
        normalized_header = [normalized_header_full[i] for i in sorted(normalized_index)]
        headers_whitelist_filter = None if headers_whitelist is None else headers_whitelist.get(file_type, None)
        ret.append(Single(filename=filename, header=normalized_header, events=normalized_events, headers_whitelist=headers_whitelist_filter))

    csv_reader = csv.reader(lines)

    for last_row_index, row in enumerate(csv_reader):
        if len(row) > 0 and row[0][:1] in header_permitted_first_chars:
            if header is not None and len(events) > 0:
                append(header=header, events=events, last_row_index=last_row_index)
            header = row
            events.clear()
        else:
            events.append(row)

    if header is not None:
        if len(events) > 0: # drop the last header row if no events came after it
            append(header=header, events=events, last_row_index=last_row_index)
        else:
            print('warn: {}: dropping last header since no events came after it'.format(filename), file=stderr)

    return ret


# TODO - move to configuration
# TODO - do we need this? right now do a check for each line
# based on the values, not the type (i.e. header name)
#NUMERICAL_COLUMNS = {
#    PUMP_FILE_TYPE = [],
#    MPPT_FILE_TYPE = [],
#}


def is_float(x):
    try:
        float(x)
    except:
        return False
    return True


def normalize_event(event, time_index, check_float):
    """
    Given time_index and header, return a normalized form of the event:

    event = [col1, col2, .., col_i, time, col_i+2, .., col_n]
    return = [time, num1, .. num_m] if not check_float, else (indices, event[indices])
    where m <= n
    and every column that isn't numerical is removed

    NOTE: right now we look at the values, anything that can be parsed
    as a floating point number remains in order, the rest are removed.
    This assumes we get the exact same variables and in the same order

    NOTE: Possible future change: have a whitelist of column names per
    file type.
    The whitelists will need to be updated from time to time
    (will have to be available via the configuration option of the webpanel)
    (add an option to read the existing header to make the webpanel able to do a
    full iteration:
     webpanel.download a file for a certain date
     (client) see missing header,
     webpanel.get headers of the raw file
     webpanel.edit configuration, download again
    """
    if check_float:
        indices = [time_index] + [i for i, x in enumerate(event) if i != time_index and is_float(x)]
        ret_event = [float(event[time_index])] + [float(x) for x in event[:time_index] + event[time_index + 1:] if is_float(x)]
        # testing - can be removed later
        #other_ret_event = [float(event[i]) for i in indices]
        #assert ret_event == other_ret_event
        return indices, ret_event
    return [event[time_index]] + event[:time_index] + event[time_index + 1:]


class Averager:
    def __init__(self):
        self._sum = 0
        self.count = 0

    def add(self, v):
        self._sum += v
        self.count += 1

    def get_and_reset(self):
        if self.count == 0:
            return nan
        r = self._sum / self.count
        self._sum = 0
        self.count = 0
        return r


def averaging_merge(singles, period):
    """
    Merging and averaging is done at the same time, after normalization
    This way we can gurantee we don't drop any data. Any events in
    [t0, t0+period) are averaged and combined to a single event

    @period: averaging time in *milliseconds*

    Return: header, merged_averaged_events

    TODO: update doc string below

    [     ]     [ ]
    AAABBBC     DDD
      [     ]  [    ]
      ABBBCCC   DDD

    ignoring the singles distinction! just merging the events

    singles = [single_0_list, single_1_list]
    events_0 = singles_0_list.events
    time_indices = [index_0, index_1]
    events_0 = [events_0_0, events_0_1, events_0_2]
    events_1 = [events_1_0, events_1_1, events_1_2]
    period = 2
    Ts:
     0, 1, 2
     0.1, 1.1, 2.1

    output returned
    [0, col_0[0, 1] avg, col_1[0, 1] avg, same forevents 1]

    yield event_0, event, is_event_0
    in order of time of events
    skipping the events of events_i i>0 where time(event) < time(events_0_0)
    time is taken given time_indices matchine events
    """

    # Convert to a single events list for each event source (base = first, and
    # rest)
    all_events_lists = [sum((s.events for s in singles_list), []) for singles_list in singles]
    #import pdb; pdb.set_trace()
    if len(singles) == 0 or len(singles[0]) == 0:
        return (['nothing_yet_try_later'], [])
    # TODO: handle changing headers - right now just barf
    first_header = singles[0][0].header
    for i, singles_list in enumerate(singles):
        header_set = set(tuple(s.header) for s in singles_list)
        if len(header_set) != 1:
            if debug: print("debug: headers not the same for singles list {i}, taking first header = {first_header}".format(
                i=i, first_header=repr(first_header)), file=stderr)
    # use time (index 0) from first list and [1:] from the rest
    header = [first_header[0]] + sum((singles_list[0].header[1:] for singles_list in singles), [])
    assert all(isinstance(x[0][0], float) for x in all_events_lists)

    N_cols = [len(events_list[0]) for events_list in all_events_lists]
    # note: we cut at the time of events[0][0]

    # running indices into events lists
    indices = [0 for _ in all_events_lists]
    Ns = [len(events) for events in all_events_lists]

    base_event = all_events_lists[0][0]
    base_time = base_event[0]

    # Skip first events to match so that:
    # T_0 <= {T_1, .., T_N_sources}
    # give a debug print on the number of dropped events
    count = 0
    for ev_i, events in enumerate(all_events_lists[1:]):
        for i in range(len(events)):
            if events[i][0] >= base_time:
                indices[ev_i + 1] = i
                break
            count += 1
    if count > 0:
        if debug: print('debug: dropped {} events because they were too early'.format(count), file=stderr)
    """
    T0_1 A0_1 B0_1
                     T1_1 C1_1
    T0_2 ..
    """
    ret_events = []
    col_map = []
    col_i = 0
    for i, N_col in enumerate(N_cols):
        for j in range(1, N_col):
            col_map.append((col_i, i, j))
            col_i += 1
    averagers = [Averager() for _ in header[1:]]
    assert len(averagers) == len(col_map), '{} != {}'.format(len(averagers), len(col_map))
    if debug: print('debug: processing {}'.format(repr(Ns)), file=stderr)
    skipped_rows_wrong_col_count = 0
    skipped_rows_backwards_time = 0
    base_rows_time_going_backwards = 0
    cutoff = base_time
    while any(c < n for c, n in zip(indices, Ns)) and indices[0] < Ns[0]:
        base_event = all_events_lists[0][indices[0]]
        if base_event[0] < cutoff:
            base_rows_time_going_backwards += 1
        base_time = max(base_time + 0.01, base_event[0]) # prevent going backwards in time
        cutoff = base_time + period
        next_indices = list(indices)
        for events_i, (events, index, N) in enumerate(zip(all_events_lists, indices, Ns)):
            for next_index in range(index, N):
                t = events[next_index][0]
                if t >= cutoff:
                    break
            else:
                next_index = N
            next_indices[events_i] = next_index
        for out_col, events_i, events_i_col in col_map:
            avg = averagers[out_col]
            events = all_events_lists[events_i]
            for row_i in range(indices[events_i], next_indices[events_i]):
                row = events[row_i]
                if events_i_col >= len(row):
                    skipped_rows_wrong_col_count += 1
                else:
                    # sanity check: time must always go forward
                    if row[0] < base_time:
                        skipped_rows_backwards_time += 1
                    else:
                        avg.add(row[events_i_col])
        event = [base_time] + [avg.get_and_reset() for avg in averagers]
        ret_events.append(event)
        if debug: print('debug: {} => {}'.format(indices, next_indices), file=stderr)
        indices = next_indices
    if skipped_rows_wrong_col_count > 0:
        print("warn: skipped rows due to wrong column count: {skipped_rows_wrong_col_count}".format(**locals()), file=stderr)
    if skipped_rows_backwards_time > 0:
        print("warn: skipped rows due to timestamp being before start of window: {skipped_rows_backwards_time}".format(**locals()), file=stderr)
    if base_rows_time_going_backwards > 0:
        print("warn: pump rows with time going backwards: {base_rows_time_going_backwards}".format(**locals()), file=stderr)
    return header, ret_events


def average_helper(start_date, end_date, period, path, headers_whitelist=None):
    """
    Return a new averaged and combined csv text.

    Complete algorithm
    1. select all files between (inclusive) start_date and end_date
     - date is computed by filename only. No test for matching of time stamps
       in file is done
    2. break each file to (header, eventlist) = single
    3. for each single, compute average over period
     - specifically, stay with the times of each single, and compute
       per single (this avoids accidentally averaging across a single gap)
    3. combine the singles by using mppt as the time source, and matching a
       sample from any other single to the closest time.
       The exact matching is logic is picking the closest higher time (note:
       leaving option of additional strategy of no merge at all, just interleaving,
       in which case the merging can be done by the user via excel/python/etc)

    see test_average for example
    """
    assert isinstance(start_date, date), "start_date is not date - {} - {}".format(start_date.__class__, repr(start_date))
    assert isinstance(end_date, date)
    assert isinstance(path, str)
    assert isinstance(period, float)
    filenames_date = get_files_in_range(start_date=start_date, end_date=end_date, path=path)
    singles = {
        file_type:
            sum((break_file_to_singles(
                    filename=filename,
                    file_type=file_type,
                    headers_whitelist=headers_whitelist) for d, filename in l), [])
        for file_type, l in filenames_date
    }
    ordered_singles = [singles[MAIN_FILE_TYPE]] + [s for file_type, s in singles.items() if file_type != MAIN_FILE_TYPE]
    return averaging_merge(ordered_singles, period)


def average(start_date, end_date, period, path, headers_whitelist=None):
    header_raw, events_raw = average_helper(
        start_date=start_date, end_date=end_date, period=period, path=path,
        headers_whitelist=headers_whitelist)
    # Performance sucks. Can be helped by avoiding allocations maybe - make this
    # all a generator, all the way down. No need to sort the lines in the files,
    # we can assume they are time monotonic.
    header, events = display_transform(header_raw, events_raw)
    return [','.join([str(a).strip() for a in x]) + '\n' for x in [header] + events]


# get timezone for saving local time in averaged file, and displayed webpanel
tz = get_localzone()


def display_transform(header_raw, events_raw):
    dmf = display_mapping_flat
    header = []
    seen = set()
    for h_orig in header_raw:
        h = dmf.get(h_orig, h_orig)
        if h in seen:
            continue
        seen.add(h)
        header.append(h)
    events = []
    for event_raw in events_raw:
        d = defaultdict(list)
        for var, value in zip(header_raw, event_raw):
            if var == 'timestamp':
                dt = tz.fromutc(datetime.fromtimestamp(value / 1000.0))
                value = '{:%Y-%m-%d %H:%M:%S}'.format(dt)
            d[dmf.get(var, var)].append(value)
        events.append([max(v) for k2, v in d.items()])
    return header, events


def from_date_str(date_str):
    year, month, day = date_str.split('-')
    return date(year=int(year), month=int(month), day=int(day))


def main():
    from argparse import ArgumentParser, ArgumentTypeError

    def DirType(s):
        if not os.path.isdir(s):
            raise ArgumentTypeError('{} is not a directory'.format(s))
        return s

    parser = ArgumentParser()
    parser.add_argument('-i', '--interval', type=float, default=60.0, help="seconds")
    parser.add_argument('-p', '--path', type=DirType, default='.')
    parser.add_argument('-t', '--today', action='store_true', default=False)
    parser.add_argument('-d', '--debug', action='store_true', default=False)
    parser.add_argument('-s', '--start', default=None)
    parser.add_argument('-e', '--end', default=None)
    args = parser.parse_args()
    if args.today:
        start_date = end_date = datetime.now().date()
    elif args.start and args.end:
        start_date, end_date = from_date_str(args.start), from_date_str(args.end)
    else:
        start_date, end_date = date(1900, 1, 1), date(2100, 1, 1)
    globals()['debug'] = args.debug
    result = average(
        start_date=start_date,
        end_date=end_date,
        period=args.interval * 1000,
        path=args.path,
        )
    for l in result:
        print(l, end='')


if __name__ == '__main__':
    main()
