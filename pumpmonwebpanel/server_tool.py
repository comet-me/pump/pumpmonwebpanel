#!/usr/bin/env python3

"""
TODO: use ssh connection pooling with a master and control
"""

from tempfile import TemporaryDirectory
from sys import stderr
from os import makedirs, system, environ, getuid
from argparse import ArgumentParser
from datetime import datetime
from subprocess import check_output, CalledProcessError
import requests
import sqlalchemy as sa
from time import sleep
from os.path import exists, join


# read local .env
if exists('.env'):
    with open('.env') as fd:
        for line in fd.readlines():
            if len(line.strip()) == 0 or line.strip()[:1] == '#':
                continue
            parts = [x.strip() for x in line.split('=', 1)]
            environ[parts[0]] = parts[1]

# Constants

REMOTE_LOGS_PATH = '/var/lib/pumpmon/logs'
SYNC_USER = 'cometmelogger' # all stations connect using this username for now. Later would be good to split it, user per machine id, plus a user that can only write files (and have reversessh user only capble of opening ports)
NEW_STATION_DEFAULT_OFFSET = 0
LOGS_ROOT = f'/home/{SYNC_USER}/pump'

SERVER_BASIC_AUTHORIZATION_USER = environ['PUMP_WEBPANEL_SERVER_BASIC_AUTHORIZATION_USER']
SERVER_BASIC_AUTHORIZATION_PASSWORD = environ['PUMP_WEBPANEL_SERVER_BASIC_AUTHORIZATION_PASSWORD']


def connect_to_db(args):
    e = sa.create_engine('postgresql://@/?host=/var/run/postgresql&port=5433')
    c = e.connect()
    return c


def ssh(offset, command):
    ssh_port = ssh_port_from_offset(offset)
    return check_output(['ssh' , '-o', 'StrictHostKeyChecking=no', '-p', f'{ssh_port}', 'pump@localhost', command]).decode()


def remote_file(offset, filename):
    return ssh(offset=offset, command=f'cat "{filename}"')


def remote_write_file(offset, filename, contents):
    """
    Returns True if file was updated, False otherwise
    """
    prev = remote_file(offset, filename)
    if prev == contents:
        debug(f'file {filename} does not need changing')
        return False
    debug(f'{filename}: PREV: {prev!r}')
    ssh(offset=offset, command=f'sudo bash -c \'cat > "{filename}" <<EOF\n{contents}\nEOF\'')
    new = remote_file(offset, filename)
    # TODO: strip should not be required. There is an extra EOL being dropped
    assert new.strip() == contents.strip(), "failed to update file"
    return True


def remote_machine_id(offset):
    return remote_file(offset, '/etc/machine-id').strip()


def ssh_port_from_offset(offset):
    assert legal_offset(offset, from_offset=True) # can be both from and to
    return 20000 + offset


def http_port_from_offset(offset):
    assert legal_offset(offset)
    return 30000 + offset


def logs_root_from_slug(slug):
    return join(LOGS_ROOT, slug, 'logs')


def legal_offset(offset, from_offset=False):
    if from_offset:
        return offset >= 0 and offset < 10000
    return offset > 0 and offset < 10000


def update_sync_env(old_offset, new_offset, subdir, username):
    """ What to set: /etc/pumpmon/sync.env
# cat /etc/pumpmon/sync.env
PUMPMON_TARGET=unconfiguredpump@logger.come-me.org:pump/unconfiguredpump/
PUMPMON_SSH_TUNNEL_REMOTE_SSH=20000
PUMPMON_SSH_TUNNEL_REMOTE_HTTP=30000
SSH_WEBPANEL_TARGET=pumpmonreversessh@logger.comet-me.org
    """
    print('updating sync.env')
    assert legal_offset(new_offset > 0), f'illegal offset {new_offset} - must be in [1, 9999]'
    assert set(subdir) <= set('abcdefghijklmnopqrstuvwxyz_-0123456789')
    assert set(username) <= set('abcdefghijklmnopqrstuvwxyz_-0123456789')
    ssh_port = ssh_port_from_offset(new_offset)
    assert not reverse_ssh_on_port(ssh_port), f'port already used: {ssh_port}'
    http_port = http_port_from_offset(new_offset)
    return remote_write_file(old_offset, filename='/etc/pumpmon/sync.env',
        contents=f"""PUMPMON_TARGET={username}@logger.comet-me.org:pump/{subdir}/
PUMPMON_SSH_TUNNEL_REMOTE_SSH={ssh_port}
PUMPMON_SSH_TUNNEL_REMOTE_HTTP={http_port}
SSH_WEBPANEL_TARGET=pumpmonreversessh@logger.comet-me.org
""")


def db_slug_to_offset(args):
    c = connect_to_db(args)
    offset = list(c.execute(sa.text('select port as offset from pump_monitors where slug = :slug'),
                            slug=args.slug))[0][0]
    if offset is None:
        offset = NEW_STATION_DEFAULT_OFFSET
    return offset


def get_next(args):
    """ return next station to deploy data """
    c = connect_to_db(args)
    work = list(c.execute('select machine_id, slug, port as offset from pump_monitors where machine_id is null or port is null'))
    if len(work) == 0:
        return None, None
    assert len(work) == 1, "no support yet for more than one outstanding work item"
    machine_id, slug, offset = work[0]
    # minor TODO - do this in a single select
    if offset is None:
        debug('selecting next offset')
        offset = list(c.execute('select max(port) + 1 from pump_monitors where machine_id is not null and port < 9000'))[0][0]
    if machine_id is not None:
        print("deploying a specific machine_id is not yet supported")
    return slug, offset



def error(s):
    print(f'error: {s}', file=stderr)
    raise SystemExit


def debug(t):
    print(f'debug: {t}')


def reverse_ssh_on_port(port):
    out = check_output(['bash', '-c', f'netstat -ltnop | grep {port} | grep -v grep; exit 0'])
    debug(out)
    return len(out) != 0


def reverse_ssh_new_available(old_offset):
    return reverse_ssh_on_port(ssh_port_from_offset(old_offset))


def update_db(args, slug, offset):
    c = connect_to_db(args)
    c.execute(f"update pump_monitors set port = {offset} where slug = '{slug}'")


def update_offset(args, old_offset, new_offset, slug):
    updated_sync_env = update_sync_env(old_offset=old_offset, new_offset=new_offset,
                                       subdir=slug, username=SYNC_USER)

    if updated_sync_env:
        try:
            ssh(old_offset, 'sudo systemctl restart pumpmon-system-ssh-tunnel')
        except CalledProcessError:
            pass # this is expected, as this command breaks the ssh session

    for i in range(10):
        try:
            ssh(new_offset, 'uptime')
            debug('successfully changed port, new reverse tunnel functional for SSH')
            break
        except:
            print('.')
            sleep(1)
    else:
        error(f'failed to connect to station after port update: {slug}, {new_offset}')

    update_db(args=args, slug=slug, offset=new_offset)


def check_website(http_port):
    # TODO - check the server one too. This checks the redirected one
    try:
        res = requests.get(f'http://localhost:{http_port}')
    except Exception as e:
        debug(f'website check failed with exception: {e}')
        return False
    return res and res.ok


def sync(args):
    """
    create sync directory under cometmelogger
    create a canary file on the target
    restart the sync service on the station
    verify the file is synced within 20 seconds
    """
    slug = args.slug
    offset = args.new_offset
    logs_dir = logs_root_from_slug(slug=slug)
    if not exists(logs_dir):
        print(f'creating sync directory {logs_dir}')
        makedirs(logs_dir)
    ssh_port = ssh_port_from_offset(offset)
    print('update packages on station')
    ssh(offset=offset, command='sudo apt update && sudo apt upgrade -y; exit 0')
    # TODO - these should be done by the package itself, but right now it's broken
    # create canary file
    now = datetime.now()
    year, month, day = now.year, now.month, now.day
    yyyy_mm_dd = f'{year:04}/{month:02}/{day:02}'
    remote_canary_path = join('/var/lib/pumpmon/logs', yyyy_mm_dd)
    canary_filename = f'canary_{datetime.now().microsecond}'
    remote_canary_filename = join(remote_canary_path, canary_filename)
    ssh(offset=offset, command='sudo systemctl enable pumpmon-system.timer && sudo systemctl enable pumpmon-sync && sudo systemctl start pumpmon-system.timer && sudo systemctl restart pumpmon-sync')
    ssh(offset=offset, command=f'sudo mkdir -p {remote_canary_path} && sudo touch {remote_canary_filename}')
    local_dir = join(logs_dir, yyyy_mm_dd)
    local_canary = join(local_dir, canary_filename)
    attempts = 60
    for i in range(attempts):
        if exists(local_canary):
            break
        print(f'{i + 1} / {attempts}', end='\r')
        sleep(1)
    else:
        error(f'failed to synchronize file from station to server {local_canary}')
    return True


def render_template(base, rep):
    for k, v in rep.items():
        base = base.replace(k, v)
    return base


first_su_system = [True]
def su_system(cmd):
    su_commands = 'run_as_root.sh'
    if getuid() != 0:
        mode = 'w+' if first_su_system[0] else 'a+'
        first_su_system[0] = False
        print(f'not root, storing command in file: {su_commands}: {cmd}')
        with open(su_commands, mode) as fd:
            fd.write(f'{cmd}\n')
    else:
        system(cmd)


def nginx_update_site(slug, http_port):
    sites_dir = '/etc/nginx/pumpmon-sites'
    new_filename = join(sites_dir, f'{slug}.conf')
    template = '/etc/nginx/templates/pump_station.conf.template'
    assert exists(template)
    with open(template) as fd:
        contents = render_template(fd.read(), {'__SLUG__': slug, '__HTTP_PORT__': str(http_port)})
    rendered_dir = 'rendered'
    if not exists(rendered_dir):
        makedirs(rendered_dir)
    tempfile = join(rendered_dir, f'{slug}.conf')
    with open(tempfile, 'w+') as fd:
        fd.write(contents)
    su_system(f'mv {tempfile} {new_filename}')
    su_system('systemctl reload nginx')
    return True


def nginx(args):
    """
    create nginx redirection from /pumps-site/<slug> to localhost:http_port
    create new pumpmonwebpanel{,-celery,-indexer} services based off 
    create nginx redirection

    http_port = 30000 + offset

    TODO: server instance: this would be simpler with docker - one for each
    station, or maybe docker-compose, or maybe kubernetes. Need to think
    """
    slug = args.slug
    http_port = http_port_from_offset(args.new_offset)
    print('TODO: update nginx for both server and site based on slug and offset')
    nginx_update_site(slug=args.slug, http_port=http_port)


def check_date_match(offset):
    # TODO check dates match - note that this will be broken in the future, when stations are in different timezones, need to check UTC date then
    format_str = '%Y%m%d%H%M%S'
    station_date_str = ssh(offset=offset, command=f'date +{format_str}').strip()
    station_date = datetime.strptime(station_date_str, format_str)
    now = datetime.now()
    larger, smaller = max(now, station_date), min(now, station_date)
    diff = (larger - smaller).seconds
    if diff > 600:
        error(f'large time difference compared to station: local {now}, station {station_date}, diff {diff} seconds')
    print(f'station/server {station_date}, {now}, diff {diff}')


def args_into_config(args):
    if not args.overwrite_db and (args.slug is not None or args.new_offset is not None):
        print('illegal combination - do not set slug nor newoffset unless overwrite-db be set')
        raise SystemExit
    if args.overwrite_db:
        slug = args.slug
        new_offset = args.new_offset
        assert slug is not None
        if new_offset is None:
            new_offset = old_offset # no offset change, but does everything else
    else:
        slug, new_offset = get_next(args)
    args.slug = slug
    if slug is None:
        return
    old_offset = db_slug_to_offset(args)
    assert new_offset is not None
    new_offset = int(new_offset)
    args.old_offset = old_offset
    args.new_offset = new_offset
    assert legal_offset(old_offset, from_offset=True)
    assert legal_offset(new_offset)
    print(f'{slug} {old_offset} => {new_offset}')
    return args


def offset(args):
    """
    Set the correct port for the machine, update it's slug and port and machine_id in
    the database, and set it's hostname accordingly.
    Restart it's webservice internally, it will show the slug
    """
    slug = args.slug
    old_offset = args.old_offset
    new_offset = args.new_offset
    print(f"DEPLOYING for slug {slug}, new offset {new_offset} (expected on {old_offset})")
    connection_available = reverse_ssh_new_available(old_offset)
    if not connection_available:
        error(f'no reverse ssh connection on port {ssh_port_from_offset(args.old_offset)}, check usual suspects')
    machine_id = remote_machine_id(old_offset)
    print(f'remote machine id: {machine_id}')
    check_date_match(offset=old_offset)

    if new_offset == old_offset:
        print('not updating sync.env, offset is unchanged')
    else:
        update_offset(args=args, old_offset=old_offset, new_offset=new_offset, slug=slug)

    update_webpanel_env(new_offset, slug)
    ssh(offset=new_offset, command='sudo systemctl restart pumpmonwebpanel.service')
    ssh_port = ssh_port_from_offset(new_offset)
    http_port = http_port_from_offset(new_offset)
    return check_website(http_port)


def update_webpanel_env(offset, slug):
    contents = f"""LOGS_PATH={REMOTE_LOGS_PATH}
SERVER_BASIC_AUTHORIZATION_USER={SERVER_BASIC_AUTHORIZATION_USER}
SERVER_BASIC_AUTHORIZATION_PASSWORD={SERVER_BASIC_AUTHORIZATION_PASSWORD}
PUMPMON_PANEL_SLUG={slug}
"""
    remote_write_file(offset=offset, filename='/etc/pumpmon/webpanel.env', contents=contents)


stages = [offset, sync, nginx]
stage_names = [x.__name__ for x in stages]


def exit(s):
    print(s)
    raise SystemExit


def main():
    parser = ArgumentParser(description='Comet-ME pump monitor server deployment tool. Currently has one funcntion: looking for a connected new station and updating it, here called deploying')
    parser.add_argument('--overwrite-db', default=False, help='debug: update row even if it does not exist or is already full in db', action='store_true')
    parser.add_argument('--slug', help='debug, required with overwrite-db')
    parser.add_argument('--new-offset', default=None, type=int, help='debug, required with overwrite-db')
    parser.add_argument('--stage', default=stage_names[0], choices=stage_names, help='starting stage')

    args = parser.parse_args()
    args = args_into_config(args)
    if args.slug is None:
        exit("no work to do - no slug in db or command line")
    for i in range(stage_names.index(args.stage), len(stages)):
        print(f'running {stage_names[i]} **********')
        if not stages[i](args):
            exit('stage failed')
            break


if __name__ == '__main__':
    main()

