#!/bin/bash
systemctl --user daemon-reload
systemctl --user restart celery
journalctl --user --follow --unit celery
