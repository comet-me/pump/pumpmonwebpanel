#!/bin/bash
MPPT=$(ssh webpanel@192.168.96.1 "ls -r /var/lib/pumpmon/logs/mppt*" | head -1)
CONT=$(ssh webpanel@192.168.96.1 "ls -r /var/lib/pumpmon/logs/pump*" | head -1)
BASE_MPPT=$(basename $MPPT)
BASE_CONT=$(basename $CONT)
echo $MPPT
echo $BASE_MPPT
echo $CONT
echo $BASE_CONT
(ssh webpanel@192.168.96.1 tail -f $MPPT >> testlogs/$BASE_MPPT) &
(ssh webpanel@192.168.96.1 tail -f $CONT >> testlogs/$BASE_CONT) &
sleep 100000
