#!/bin/bash
cd /var/lib/pumpmon/logs
watch "(tail -100 pump_$(date +%Y%m%d).csv  | tr ',' '\t' | column -t)"
