/*
import os
import sys
from glob import glob
from datetime import date
from collections import defaultdict
from math import nan
*/
extern crate argparse;
extern crate csv;

use std::path::Path;
use std::fs;
use std::collections::HashMap;
use std::collections::HashSet;
use std::hash::Hash;

use argparse::{ArgumentParser, Store};


// TODO: use some standard date representation
#[derive(Debug, Eq, PartialEq, PartialOrd, Ord, Copy, Clone)]
struct MyDate {
    year: i32,
    month: i32,
    day: i32,
    hour: i32,
    minute: i32,
}

impl MyDate {
    fn new(year: i32, month: i32, day: i32, hour: i32, minute: i32) -> MyDate {
        MyDate { year: year, month: month, day: day, hour: hour, minute: minute }
    }

    fn between(&self, lower: &MyDate, upper: &MyDate) -> bool
    {
        self >lower && upper >self
    }
}


#[derive(Debug)]
enum AveragerError {
    FileParser(String)
}


fn _parse_filename(f: &String) -> Result<(MyDate, String), AveragerError> {
    //
    //Helper for get_files_in_range
    //
    let path = Path::new(f);
    let filename = path.file_stem().unwrap().to_str().unwrap(); // os.path.splitext(os.path.basename(f))[0]
    let parts: Vec<&str> = filename.split('_').collect();
    if parts.len() < 2 {
        return Err(AveragerError::FileParser(format!("not enough filename parts - {}, {}, {:?}", f, filename, parts)));
    }
    let (file_type, file_date, third_part) = (parts[0], parts[1], parts.get(2).unwrap_or(&"0000"));
    if file_date.len() != 8 {
        return Err(AveragerError::FileParser(format!("file_date is not 8 bytes: {}", file_date)));
    }
    let file_time = {
        if third_part.len() >= 4 {
            third_part
        } else {
            &"0000"
        }
    };
    let (year_str, month_str, day_str, hour_str, minute_str) =
        (&file_date[..4], &file_date[4..6], &file_date[6..8], &file_time[..2], &file_time[2..4]);
    let d = MyDate {
        year: String::from(year_str).parse().unwrap(),
        month: String::from(month_str).parse().unwrap(),
        day: String::from(day_str).parse().unwrap(),
        hour: String::from(hour_str).parse().unwrap(),
        minute: String::from(minute_str).parse().unwrap(),
    };
    Ok((d, file_type.to_string()))
}

// NOTE: These must be the prefixes used in the saved files (i.e. currently
// identity of the X_FILE_TYPE and the X prefix is assumed)
const PUMP_FILE_TYPE : &str = "pump";
const MPPT_FILE_TYPE : &str = "mppt";

// This is the file_type used for STRATEGY_CLOSEST_TO_0 as the time source.
// See doc string of <merge_events> below
const MAIN_FILE_TYPE : &str = PUMP_FILE_TYPE;

// For each new file_type, add the header for the timestamp. Timestamp is
// milliseconds since epoch.
// py2rust: would use a const HashMap but requires experimental const fn, so
// seems easier to use a function for now
fn TIME_HEADERS(k: &str) -> Option<&str> {
    match k {
        PUMP_FILE_TYPE => Some("timestamp"),
        MPPT_FILE_TYPE => Some("RecordedTime"),
        _ => None,
    }
}


fn glob_star_extension(path: &str, extension: &str) -> Vec<String>
{
    let mut ret = vec![];
    for entry in fs::read_dir(path).unwrap() {
        if let Ok(entry) = entry {
            let p = entry.path();
            if let Some(ext) = p.extension() {
                if ext == extension {
                    ret.push(String::from(p.to_str().unwrap()));
                }
            }
        }
    }
    ret
}

// py2rust: default dictionary


fn get_files_in_range(start_date: &MyDate, end_date: &MyDate, path: &str)
    -> Vec<(String, Vec<(MyDate, String)>)> {
    /*
    Reads csv files from path, and based on the encoded date in the path
    returns a list of tuples
     (file_type, [(date, filename)])

    The list is sorted by date from first to last

    The date is encoded in the filename as:

    FILETYPE_YYYYMMDD[_C*].csv

    Where C is a consecutive number, lowest is earliest
    */
    let all_filenames = glob_star_extension(path, "csv");
    let mut ret_dicts : HashMap<String, Vec<(MyDate, String)>> = HashMap::new(); // py: defaultdict(list)
    let mut main = vec![];
    for filename in all_filenames {
        let base_filename = String::from(Path::new(&filename).file_name().unwrap().to_str().unwrap());
        let parse_result = _parse_filename(&base_filename);
        if parse_result.is_err() {
            match parse_result {
                Err(AveragerError::FileParser(s)) => println!("{} parsing failed: {}", filename, s),
                _ => (),
            }
            continue;
        }
        let (d, file_type) = parse_result.unwrap();
        println!("progress: {:?} {} from {}", d, file_type, filename);
        if !d.between(start_date, end_date) { // rust todo: operators
            println!("debug: ! {:?} < {:?} < {:?}", start_date, d, end_date);
            continue;
        }
        if file_type == MAIN_FILE_TYPE {
            main.push((d, filename));
        } else {
            if ret_dicts.get(&file_type).is_none() {
                ret_dicts.insert(file_type, vec![(d, filename)]);
            } else {
                ret_dicts.get_mut(&file_type).unwrap().push((d, filename));
            }

        }
    }

    let rest : Vec<(String, Vec<(MyDate, String)>)> =
        ret_dicts.iter()
            .map(|(file_type, l)| ((*file_type).clone(), {
                let mut l2 = (*l).clone();
                l2.sort_unstable(); // TODO - should not generate a copy
                l2
            })).collect();
        // [(file_type, list(sorted(l))) for file_type, l in ret_dicts.items()]
    main.sort_unstable();
    let mut ret = vec![(String::from(MAIN_FILE_TYPE), main)];
    ret.extend(rest);
    println!("progress: files_in_range: {:?}", ret);
    ret
}

// TOOD: rust: better way? equiv to py: [1,2,3].index(2)
fn index<T: std::cmp::PartialEq>(s: &Vec<T>, v: &T) -> Option<usize> {
    for (i, _v) in s.iter().enumerate() {
        if v == _v {
            return Some(i);
        }
    }
    None
}


fn time_index_from_header(file_type: &str, header: &Vec<String>) -> Option<usize> {
    let time_header = TIME_HEADERS(file_type).unwrap();
    index(header, &String::from(time_header))
}


#[derive(Debug)]
struct Single {
    filename: String,
    header: Vec<String>,
    events: Vec<Vec<f32>>
}

impl Clone for Single {
    fn clone(&self) -> Single
    {
        Single {
            filename: self.filename.clone(),
            header: self.header.clone(),
            events: self.events.clone()
        }
    }
}

impl Single {
    fn new(filename: &str, header: &Vec<String>, events: &Vec<Vec<f32>>) -> Single {
        // sanity check: all events have the same length
        // py2rust: how to do len(set(x))
        let events_len = events[0].len();
        for others in &events[1..] {
            assert!(others.len() == events_len,
                    "non rectangular Single {}, {} != {}", filename, events_len, others.len());
        }
        Single { filename: filename.to_string(), header: header.to_vec(), events: events.to_vec() }
    }
/*
    def __str__(self):
        return f'<Single {self.filename} {self.header!r}>'

    def __repr__(self):
        return f'<Single {self.filename} {self.header!r}>'
        */
}

/* TODO: this is a hack, a line can start with an enumeration and in that case it
 * will not necessarily fit this test for headerness. Switch to having one header
 * per CSV, which fixes this ambiguity
 */
fn is_header(candidate: &String) -> bool
{
    let first = candidate.chars().nth(0).unwrap();
    first.is_ascii_alphabetic()
}


fn hist<T: Iterator, U: Hash + Eq + Copy>(l: T, f: &Fn(&T::Item) -> U) -> (HashMap<U, u32>, HashMap<U, T::Item>) {
    let mut ret : HashMap<U, u32> = HashMap::new();
    let mut m : HashMap<U, T::Item> = HashMap::new();
    for item in l {
        let res = (&f)(&item);
        let res_new = ret.get(&res).unwrap_or(&0) + 1;
        ret.insert(res, res_new);
        m.insert(res, item);
    }
    (ret, m)
}


fn break_file_to_singles(filename: &str, file_type: &String) -> Vec<Single> {
    let mut ret: Vec<Single> = vec![];
    let mut events: Vec<Vec<String>> = vec![];
    let mut header: Option<Vec<String>> = None;

    let single = |events: &Vec<Vec<String>>, header: &Vec<String>| -> Single {
        let time_index = time_index_from_header(file_type, header).unwrap();
        let (_, normalized_header_full) = normalize_event_to_strings(header, time_index);
        let normalized_events_and_indices: Vec<(Vec<usize>, Vec<f32>)> = events.iter().map(|event| normalize_event_to_floats(event, time_index)).collect();
        let normalized_events: Vec<Vec<f32>> = normalized_events_and_indices.iter().map(|&(_, ref es)| es.clone()).collect();
        let normalized_indices: HashSet<Vec<usize>> =
            normalized_events_and_indices.iter().map(|&(ref indices, _)| indices.clone()).collect();
        let mut normalized_index: Vec<usize> = if normalized_indices.len() != 1 {
            // in case of inconsistencies, keep the biggest piece
            panic!("TODO");
            /*
            temp_d, temp_map = hist(normalized_events_and_indices, lambda indices_es: len(indices_es[0]))
            inv_temp_d = {v: k for k, v in temp_d.items()}
            _len = inv_temp_d[max(temp_d.values())]
            // drop all events that do not match
            dropped_indices = [i for i, ev in enumerate(normalized_events) if len(ev) != _len]
            eprintln!("debug: dropping part of single of {}: {:?} (last line index = {})",
                      filename, dropped_indices, last_line_index);
            for i in reversed(dropped_indices):
                del normalized_events[i]
            normalized_index = temp_map[_len][0]
            */
        } else {
            normalized_indices.iter().next().unwrap().clone()
        };
        normalized_index.sort_unstable();
        println!("break: {}: normalized_index: {:?}", filename, normalized_index);
        let mut normalized_header = vec![];
        for i in normalized_index.iter() {
            normalized_header.push(normalized_header_full[*i].clone());
        }
        //let normalized_header = normalized_index.iter().map(|&i| normalized_header_full[i]).collect(); // E0277
        // drop lines that are not floating point ASCII representations

        Single::new(filename, &normalized_header, &normalized_events)
    };

    let mut reader = csv::Reader::from_file(filename).unwrap().has_headers(false).flexible(true);
    println!("progress: reading {}", filename);
    for (_record_i, record_opt) in reader.decode::<Vec<String>>().enumerate() {
        let record : &Vec<String> = &record_opt.unwrap();
        if record.len() > 0 && is_header(&record[0]) {
            if let Some(hdr) = header {
                if events.len() > 0 {
                    ret.push(single(&events, &hdr));
                }
            }
            events.clear();
            header = Some(record.to_vec());
        } else {
            events.push(record.to_vec());
        }
    }

    if header != None {
        if events.len() > 0 { // drop the last header line if no events came after it
            if let Some(hdr) = header {
                ret.push(single(&events, &hdr));
            }
        } else {
            eprintln!("debug: {}: dropping last header since no events came after it", filename);
        }
    }
    println!("progress: got {} singles", ret.len());
    ret
}


// TODO - move to configuration
// TODO - do we need this? right now do a check for each line
// based on the values, not the type (i.e. header name)
//NUMERICAL_COLUMNS = {
//    PUMP_FILE_TYPE = [],
//    MPPT_FILE_TYPE = [],
//}


fn is_float(x: &str) -> bool {
    x.parse::<f32>().is_ok()
}


fn normalize_event_to_floats(event: &Vec<String>, time_index: usize) -> (Vec<usize>, Vec<f32>) {
    /*
    Given time_index and header, return a normalized form of the event:

    event = [col1, col2, .., col_i, time, col_i+2, .., col_n]
    return = (indices, event[indices])
    where m <= n
    and every column that isn't numerical is removed

    NOTE: right now we look at the values, anything that can be parsed
    as a floating point number remains in order, the rest are removed.
    This assumes we get the exact same variables and in the same order

    NOTE: Possible future change: have a whitelist of column names per
    file type.
    The whitelists will need to be updated from time to time
    (will have to be available via the configuration option of the webpanel)
    (add an option to read the existing header to make the webpanel able to do a
    full iteration:
     webpanel.download a file for a certain date
     (client) see missing header,
     webpanel.get headers of the raw file
     webpanel.edit configuration, download again
    */
    let mut indices = vec![time_index];
    indices.extend(event.iter().enumerate()
        .filter(|&(i, x)| is_float(x) && i != time_index)
        .map(|(i, _x)| i));
    let mut ret_event = vec![event[time_index].parse::<f32>().unwrap()];
    ret_event.extend(event[..time_index].iter().filter(|x|is_float(x)).map(|x| x.parse::<f32>().unwrap()));
    ret_event.extend(event[time_index + 1..].iter().filter(|x|is_float(x)).map(|x| x.parse::<f32>().unwrap()));
    // testing - can be removed later
    //other_ret_event = [float(event[i]) for i in indices]
    //assert ret_event == other_ret_event
    (indices, ret_event)
}


fn normalize_event_to_strings(event: &Vec<String>, time_index: usize) -> (Vec<usize>, Vec<String>) {
    let mut norm = vec![event[time_index].clone()];
    norm.extend_from_slice(&event[..time_index]);
    norm.extend_from_slice(&event[time_index + 1..]);
    (vec![], norm)
}


struct Averager {
    _sum: f32,
    count: i32,
}

impl Averager {
    fn new() -> Averager {
        Averager { _sum: 0.0f32, count: 0 }
    }

    fn add(&mut self, v: f32) {
        self._sum += v;
        self.count += 1;
    }

    fn get_and_reset(&mut self) -> f32 {
        if self.count == 0 {
            return std::f32::NAN;
        }
        let r = self._sum / self.count as f32;
        self._sum = 0.0;
        self.count = 0;
        r
    }
}


fn to_set<T: Iterator>(it: T) -> HashSet<T::Item>
    where T::Item: Eq, T::Item: Hash
{
    let mut s = HashSet::new();
    for x in it {
        s.insert(x);
    }
    s
}


fn averaging_merge(singles: Vec<Vec<Single>>, period: f32) -> (Vec<String>, Vec<Vec<f32>>) {
    /*
    Merging and averaging is done at the same time, after normalization
    This way we can guarantee we don't drop any data. Any events in
    [t0, t0+period) are averaged and combined to a single event

    @period: averaging time in *milliseconds*

    Return: header, merged_averaged_events

    TODO: update doc string below

    [     ]     [ ]
    AAABBBC     DDD
      [     ]  [    ]
      ABBBCCC   DDD

    ignoring the singles distinction! just merging the events

    singles = [single_0_list, single_1_list]
    events_0 = singles_0_list.events
    time_indices = [index_0, index_1]
    events_0 = [events_0_0, events_0_1, events_0_2]
    events_1 = [events_1_0, events_1_1, events_1_2]
    period = 2
    Ts:
     0, 1, 2
     0.1, 1.1, 2.1

    output returned
    [0, col_0[0, 1] avg, col_1[0, 1] avg, same forevents 1]

    yield event_0, event, is_event_0
    in order of time of events
    skipping the events of events_i i>0 where time(event) < time(events_0_0)
    time is taken given time_indices matchine events
    */

    // Convert to a single events list for each event source (base = first, and
    // rest)
    // TODO: flat_map
    println!("averaging merge with period = {}", period);
    let mut all_events_lists : Vec<Vec<Vec<f32>>> = vec![];
    let mut header = vec![singles[0][0].header[0].clone()];
    //let all_events_lists :  = singles.iter().map(|singles_list| singles_list.iter().flat_map(|s| s.events)).collect();
    for (i, singles_list) in singles.iter().enumerate() {

        // TODO: handle changing headers - right now just barf
        let u = to_set(singles_list.iter().map(|s| s.header.clone()));
        assert!(u.len() == 1, "headers not the same for singles list {} ({:?})", i, u);

        let mut all_events : Vec<Vec<f32>> = vec![];
        for s in singles_list {
            for event in s.events.iter() {
                all_events.push(event.clone());
            }
            // cannot move out of borrows content: s.events is a vec<vec<f32>>
            //all_events.extend(s.events);
        }
        all_events_lists.push(all_events);
        header.extend_from_slice(&singles_list[0].header[1..]);
    }

    let N_cols: Vec<usize> = all_events_lists.iter().map(|events_list| events_list[0].len()).collect();
    // note: we cut at the time of events[0][0]

    // running indices into events lists
    let mut indices: Vec<usize> = all_events_lists.iter().map(|_| 0).collect();
    let Ns : Vec<usize> = all_events_lists.iter().map(|events| events.len()).collect();

    let base_event = &all_events_lists[0][0];
    let base_time = base_event[0];

    // Skip first events to match so that:
    // T_0 <= {T_1, .., T_N_sources}
    // give a debug print on the number of dropped events
    let mut count = 0;
    for (ev_i, events) in all_events_lists[1..].iter().enumerate() {
        for i in 0..events.len() {
            if events[i][0] >= base_time {
                indices[ev_i + 1] = i;
                break;
            }
            count += 1;
        }
    }
    if count > 0 {
        println!("warning: dropped {} events because they were too early", count);
    }
    /*
    T0_1 A0_1 B0_1
                     T1_1 C1_1
    T0_2 ..
    */
    let mut ret_events = vec![];
    let mut col_map = vec![];
    let mut col_i = 0;
    for (i, N_col) in N_cols.iter().enumerate() {
        for j in 1..*N_col {
            col_map.push((col_i, i, j));
            col_i += 1;
        }
    }
    let mut averagers : Vec<Averager> = (1..header.len()).map(|_| Averager::new()).collect();
    assert!(averagers.len() == col_map.len(), "{} != {}", averagers.len(), col_map.len());
    eprintln!("debug: processing {:?}", Ns);

    while indices.iter().zip(&Ns).any(|(&c, n)| c < *n) && indices[0] < Ns[0] {
        let base_event = &all_events_lists[0][indices[0]];
        let base_time = base_event[0];
        let cutoff = base_time + period;
        let mut next_indices = indices.clone();
        for (events_i, ((events, &index), &N)) in all_events_lists.iter().zip(&indices).zip(&Ns).enumerate() {
            let mut next_index = N;
            if index <= N {
                for next_index in index..N {
                    let t = events[next_index][0];
                    if t >= cutoff {
                        break;
                    }
                }
            }
            next_indices[events_i] = next_index;
        }
        for &(out_col, events_i, events_i_col) in &col_map {
            let mut avg = averagers.get_mut(out_col).unwrap();
            let events = &all_events_lists[events_i];
            for row_i in indices[events_i]..next_indices[events_i] {
                avg.add(events[row_i][events_i_col]);
            }
        }
        let mut event = vec![base_time];
        event.extend(averagers.iter_mut().map(|avg| avg.get_and_reset()));
        ret_events.push(event);
        eprintln!("debug: {:?} => {:?}", indices, next_indices);
        indices = next_indices;
    }
    (header, ret_events)
}


fn average(start_date: MyDate, end_date: MyDate, period: f32, path: &String) -> Vec<String> {
    /*
    Return a new averaged and combined csv text.

    Complete algorithm
    1. select all files between (inclusive) start_date and end_date
     - date is computed by filename only. No test for matching of time stamps
       in file is done
    2. break each file to (header, eventlist) = single
    3. for each single, compute average over period
     - specifically, stay with the times of each single, and compute
       per single (this avoids accidentally averaging across a single gap)
    3. combine the singles by using mppt as the time source, and matching a
       sample from any other single to the closest time.
       The exact matching is logic is picking the closest higher time (note:
       leaving option of additional strategy of no merge at all, just interleaving,
       in which case the merging can be done by the user via excel/python/etc)

    see test_average for example
    */
    let filenames_date = get_files_in_range(&start_date, &end_date, path);
    let mut singles = HashMap::new();
    for (file_type, l) in filenames_date {
        let mut parts = vec![];
        // TODO - progress to include headers_whitelist
        for (_d, filename) in l {
            parts.extend(break_file_to_singles(&filename, &file_type));
        }
        singles.insert(file_type, parts);
    }
    let mut ordered_singles = vec![singles[MAIN_FILE_TYPE].clone()];
    let mut not_main = vec![];
    for (file_type, s) in singles {
        if file_type == MAIN_FILE_TYPE {
            continue;
        }
        not_main.push(s);
    }
    // TODO - rustic: how to do the map and collection in a single step
    //let not_main = singles.iter()
    //    .filter(|&(file_type, s)| file_type != MAIN_FILE_TYPE)
    //    .map(|(file_type, s)| s).collect();
    ordered_singles.extend(not_main);
    let (header, events) = averaging_merge(ordered_singles, period);
    let mut combined = vec![header];
    combined.extend(events.iter().map(|es| es.iter().map(|f|f.to_string()).collect()));
    combined.iter().map(|x| format!("{}\n", {
        let q: Vec<&str> = x.iter().map(|a| a.trim()).collect();
        q.join(",")
    })).collect()
}


fn main() {
    /*
    from argparse import ArgumentParser, ArgumentTypeError

    def DirType(s):
        if not os.path.isdir(s):
            raise ArgumentTypeError(f'{s} is not a directory')
        return s

    parser = ArgumentParser()
    parser.add_argument('-i', '--interval', type=float, default=60.0)
    parser.add_argument('-p', '--path', type=DirType, default='.')
    args = parser.parse_args()
    */
    let mut interval = 1.0f32; // TODO args.interval
    let mut path = ".".to_string(); // TODO args.path
    {
        let mut ap = ArgumentParser::new();
        ap.set_description("average and concatenate pump recordings");
        ap.refer(&mut interval)
            .add_option(&["-i", "--interval"], Store, "seconds per row");
        ap.refer(&mut path)
            .add_option(&["-p", "--path"], Store, "path of logs");
        ap.parse_args_or_exit();
    }
    let result = average(
        MyDate::new(2018, 3, 25, 23, 59),
        MyDate::new(2018, 3, 27, 0, 0),
        interval * 1000.0,
        &path);
    println!("{}", result.join(""));
}
