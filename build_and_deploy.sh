#!/bin/bash

# start by building here - catches non compilable code
pip install -e . || exit -1
cd ../debian-pumpmonwebpanel
build_nspawn && ssh root@pumpzt "update_cometme_repository.sh && apt-get upgrade -y && systemctl restart pumpmonwebpanel && systemctl restart pumpmonwebpanel-indexer && systemctl restart pumpmonwebpanel-celery"
