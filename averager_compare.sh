#!/bin/bash
FIRST=$1
LAST=$2
function run()
{
    time pumpmon-average -i 60 -s 2018-$FIRST -e 2018-$LAST --debug-section memory --debug-section dates -p testlogs $@
}
run > 1.csv
run -H > 2.csv
ls -l 1.csv 2.csv
diff -s 1.csv 2.csv | head
