#!/bin/bash
MPPT=$(ssh webpanel@pumpzt "ls -r /var/lib/pumpmon/logs/mppt*" | head -1)
CONT=$(ssh webpanel@pumpzt "ls -r /var/lib/pumpmon/logs/pump*" | head -1)
BASE_MPPT=$(basename $MPPT)
BASE_CONT=$(basename $CONT)
echo $MPPT
echo $BASE_MPPT
echo $CONT
echo $BASE_CONT
OUT_MPPT=testlogs/$BASE_MPPT
OUT_CONT=testlogs/$BASE_CONT

if [ ! -e "$OUT_MPPT" ]; then
    ssh webpanel@pumpzt head $MPPT > $OUT_MPPT; # get header
fi

if [ ! -e "$OUT_CONT" ]; then
    ssh webpanel@pumpzt head $CONT > $OUT_CONT; # get header
fi

(ssh webpanel@pumpzt tail -f $MPPT >> $OUT_MPPT) &
(ssh webpanel@pumpzt tail -f $CONT >> $OUT_CONT) &
sleep 100000
