#!/usr/bin/env python3

#    pumpmonwebpanel - Comet-ME Water pump web monitoring
#    Copyright (C) 2018 Comet-ME
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

from setuptools import setup

from pumpmonwebpanel import VERSION

setup(
    name='pumpmonwebpanel',
    description='Pump monitoring web interface',
    version='.'.join(map(str, VERSION)),
    setup_requires=[],
    install_requires=[
        'flask',
        'tzlocal',
        'arrow',
    ],
    include_package_data=True,
    packages=['pumpmonwebpanel'],
    data_files=['pumpmonwebpanel/templates/root.html', "LICENSE.txt"],
    entry_points={
        'console_scripts': [
            'pumpmon-average = pumpmonwebpanel.average:main',
            'pumpmon-average-old = pumpmonwebpanel.average_old:main',
            'pumpmon-replay = pumpmonwebpanel.replay:main',
            'pumpmon-indexer = pumpmonwebpanel.indexer:main',
            'pumpmon-daily-flow = pumpmonwebpanel.tool:summarize_daily_flow',
            'pumpmon-sql-rebuild = pumpmonwebpanel.sql:rebuild',
            'pumpmon-sql-plot = pumpmonwebpanel.minuteplot:main',
            'pumpmon-migrate-to-new-dir-structure = pumpmonwebpanel.migrate_to_new_dir_structure:main',
        ]
    },
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Environment :: Console',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: GNU Affero General Public License version 3',
        'Natural Language :: English',
        'Programming Language :: Python :: 3',
        'Topic :: Utilities',
    ],
)
