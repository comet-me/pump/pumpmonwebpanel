#!/bin/bash

export FLASK_APP=pumpmonwebpanel/main.py
export PUMPMON_LOGS=$(pwd)/testlogs # TODO - replace by PUMPMON_LOGS
export PUMPMON_LOGS=$PUMPMON_LOGS
export PUMPMON_VARS=etc/vars.csv
export PUMPMON_ELF=etc/pump_drive_tiva.out
flask run "$@"
