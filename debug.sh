#!/bin/bash

export FLASK_DEBUG=1
export FLASK_APP=pumpmonwebpanel/main.py
export PUMPMON_LOGS=$(pwd)/testlogs
export PUMPMON_VARS=etc/vars.csv
export PUMPMON_ELF=etc/pump_drive_tiva.out
flask run "$@"
