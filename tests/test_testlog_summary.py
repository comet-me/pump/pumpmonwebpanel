from pathlib import Path
from datetime import datetime

from pumpmonwebpanel.average import average
from pumpmonwebpanel.consts import headers_whitelist


test_logs = (Path(__file__).parent.parent / 'testlogs').absolute()
assert test_logs.exists()
assert (test_logs / '2019' / '04' / '26' / 'pump_20190426_0606.csv').exists()


def do_average(remove_under_voltage):
    # Arrange
    root = str(test_logs)
    start_date = datetime(year=2019, month=4, day=26, hour=0, minute=0, second=0)
    end_date = datetime(year=2019, month=4, day=26, hour=23, minute=59, second=59)
    period_seconds = 60

    kw = dict(
        start_date=start_date,
        end_date=end_date,
        period_seconds=period_seconds,
        root=root,
        headers_whitelist=headers_whitelist,
        use_display_map=True,
        verbose=False,
        time_columns_names=None,
        average_period_minimal_fraction=0.0,
        drop_below_zero=False,
    )

    if remove_under_voltage is not None:
        kw['remove_under_voltage'] = remove_under_voltage

    # Act
    return average(**kw)


def check_under_voltage_removed_summary(df):
    n = df.shape[0]
    assert n == 762
    assert 'Flgs.UV' in df.columns
    assert (df['Flgs.UV'] == 0.0).value_counts().to_dict() == {True: n}


def check_under_voltage_left_in_summary(df):
    assert df.shape[0] == 770
    assert 'Flgs.UV' in df.columns
    assert (df['Flgs.UV'] == 0.0).value_counts().to_dict() == {True: 762, False: 8}


def test_testlogs_summary_no_under_voltage():
    df = do_average(remove_under_voltage=True)
    check_under_voltage_removed_summary(df)


def test_testlogs_summary_with_undervoltage():
    df = do_average(remove_under_voltage=False)
    check_under_voltage_left_in_summary(df)


def test_testlogs_summary():
    df = do_average(remove_under_voltage=None)
    check_under_voltage_left_in_summary(df)

