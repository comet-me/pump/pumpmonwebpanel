#!/usr/bin/env python

import os
import sys

os.environ['PUMPMON_LOGS'] = os.path.join(os.getcwd(), 'testlogs')

from pumpmonwebpanel.indexer import main

sys.argv = ['--debug'] + sys.argv[1:]
main()
